//
//  AboutWindowController.m
//  iDream
//
//  Created by Sébastien Stormacq on 25/11/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import "AboutWindowController.h"


@implementation AboutWindowController

- (void)awakeFromNib {
}

- (void)showAboutBox {

	[self.window center];
	
	NSDictionary* info = [[NSBundle mainBundle] infoDictionary];
	
	[versionNumber setStringValue:[info objectForKey:@"CFBundleShortVersionString"]];
	[buildNumber setStringValue:[info objectForKey:@"CFBundleVersion"]];
	
	[self.window makeKeyAndOrderFront:self];
}

@end
