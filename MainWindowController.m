//
//  MainWindowController.m
//  iDream
//
//  Created by Sébastien Stormacq on 23/11/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import "MainWindowController.h"
#import "log.h"

#define PREFS_KEY_WINDOW			@"window"				//prefs key for window position and size

@implementation MainWindowController
	
@synthesize dbManager;

// -------------------------------------------------------------------------------
//	awakeFromNib:
// -------------------------------------------------------------------------------
- (void)awakeFromNib
{
	XDebugLog(@"MainWindowControler - awakeFromNib");
	indicateProgressCount = 0;
	
}	

// -------------------------------------------------------------------------------
//	windowDidMove:
//
//  Trap the new coordinates to save it as preference
// -------------------------------------------------------------------------------
-(void)windowDidMove:(NSNotification*)notification {
	[self.window saveFrameUsingName:PREFS_KEY_WINDOW];
}

// -------------------------------------------------------------------------------
//	windowDidResize:
//
//  Trap the new coordinates to save it as preference
// -------------------------------------------------------------------------------
-(void)windowDidResize:(NSNotification*)notification {
	[self.window saveFrameUsingName:PREFS_KEY_WINDOW];
}

// -------------------------------------------------------------------------------
//	updateStatusWithMessage:
//
//  Called by the DreamboxManager when an updated status must be displayed
// -------------------------------------------------------------------------------
-(void)updateStatusWithMessage:(NSString*)msg {
	[headerViewController setStatus:msg];
}

// -------------------------------------------------------------------------------
//	updateStatusWithMessage:
//
//  Called by the DreamboxManager when the progress indicator must be updated
// -------------------------------------------------------------------------------

-(void)indicateProgress:(BOOL)show {
	
	if (show)
		indicateProgressCount++;
	else 
		indicateProgressCount--;
	
	if (indicateProgressCount > 0) {
		XDebugLog(@"++++++++++++ Animating Wait Spinner (count is %d)", indicateProgressCount);
		[headerViewController setProgressStatus:YES];
	} else { 
		XDebugLog(@"++++++++++++ Stopping Wait Spinner (count is %d)", indicateProgressCount);
		[headerViewController setProgressStatus:NO];
	}
}

// -------------------------------------------------------------------------------
//	performReadAndUpdateDreamBoxInfo:
// -------------------------------------------------------------------------------
-(void)performReadAndUpdateDreamBoxInfo {
	NSDictionary* dbInfo = [dbManager dbInfo];
	[headerViewController setDreamboxInfo:dbInfo];
}

// -------------------------------------------------------------------------------
//	applicationDidFinishLaunching:
//
//  Called by the application when launch is completed
//  Use this method to initialize the header part of the GUI
// -------------------------------------------------------------------------------
-(void)applicationDidFinishLaunching {

	channelsViewController.dbManager = dbManager;
	
	//populate header info
	[NSThread detachNewThreadSelector:@selector(performReadAndUpdateDreamBoxInfo) toTarget:self withObject:nil];
	//[self performSelectorOnMainThread:@selector(performReadAndUpdateDreamBoxInfo) withObject:nil waitUntilDone:NO];

	//populate sidebar info
	[NSThread detachNewThreadSelector:@selector(populateOutlineContents:) toTarget:channelsViewController withObject:nil];
	//[channelsViewController performSelectorOnMainThread:@selector(populateOutlineContents:) withObject:nil waitUntilDone:NO];
	
	//[[self window] display];

	

}
	
@end
