//
//  ChannelsViewController.m
//  iDream
//
//  Created by Sébastien Stormacq on 21/11/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import "ChannelsViewController.h"
#import "log.h"

@implementation ChannelsViewController 


@synthesize dbManager, contents;

#pragma mark utilities

- (SidebarNode*) selectedNode {

	return [sidebar selectedNode];
}

// -------------------------------------------------------------------------------
//	reloadTable:
//
// -------------------------------------------------------------------------------
- (void)reloadTable {
	
	[serviceTable reloadData];
	
	[lbNumberOfServices setHidden:NO];
	[lbNumberOfServices setStringValue:[NSString stringWithFormat:@"Displaying %d services", [serviceData count]]];
	
	
	
	// scroll to the top in case the outline contents is very long
	[[[serviceTable enclosingScrollView] verticalScroller] setFloatValue:0.0];
	[[[serviceTable enclosingScrollView] contentView] scrollToPoint:NSMakePoint(0,0)];	
	
}

// -------------------------------------------------------------------------------
//	applySearch:
//
// -------------------------------------------------------------------------------
-(void)applySearch {	 
	
	NSString* searchValue = [[searchField stringValue] uppercaseString];
	XDebugLog(@"Search - value = %@", searchValue);
	
	if (!initialData) {
		XDebugLog(@"Starting a new earch, copying the data");
		initialData = [NSArray arrayWithArray:serviceData];
	}
	
	if ([searchValue length] == 0) {
		
		//search is over, reset to initial data
		XDebugLog(@"End of search, return to original data set");
		serviceData = [NSMutableArray arrayWithArray:initialData];
		
	} else {
		
		NSMutableArray* selectedChannels = [[NSMutableArray alloc] init];
		
		for (NSDictionary *channel in initialData) {
			
			if ([[[channel objectForKey:DB_SERVICE_NAME] uppercaseString] rangeOfString:searchValue].location != NSNotFound) {
				[selectedChannels addObject:channel];
			}
		}
		serviceData = selectedChannels;
	}
	
	[self reloadTable];	
}

// -------------------------------------------------------------------------------
//	sortChannelsData:forKey:ascendingOrder
//
//  sort the data in the serviceData table accoring to NSDictionary's key and order
// -------------------------------------------------------------------------------
-(NSMutableArray*)sortChannelsData:(NSArray*)source forKey:(NSString*)dictKey ascendingOrder:(BOOL)ascending{
	
	// Note the use of the localizedCaseInsensitiveCompare: selector
	NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:dictKey ascending:ascending selector:@selector(localizedCaseInsensitiveCompare:)];
	
	NSArray *descriptors = [NSArray arrayWithObjects:descriptor, nil];
	return [NSMutableArray arrayWithArray:[serviceData sortedArrayUsingDescriptors:descriptors]];
}

// -------------------------------------------------------------------------------
//	applySort:
//
// -------------------------------------------------------------------------------
-(void)applySort {	
	
	switch (columnSorted) {
		case 0:
			serviceData = [self sortChannelsData:serviceData forKey:DB_SERVICE_NAME ascendingOrder:ascendingOrder];
			break;
		case 1:
			serviceData = [self sortChannelsData:serviceData forKey:DB_SERVICE_PROVIDER ascendingOrder:ascendingOrder];
			break;
		case 2:
			serviceData = [self sortChannelsData:serviceData forKey:DB_SERVICE_SAT ascendingOrder:ascendingOrder];
			break;
		default:
			break;
	}
	[self reloadTable];
} 



#pragma mark TableView Data Source

- (NSInteger)numberOfRowsInTableView:(NSTableView *)aTableView {
	
	NSInteger i = [serviceData count]; 
	//NSLog(@"numberOfRowsInTableView : %d", i);
	return i;
}

- (id)tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex {

	NSString* result;
	
	if ([[aTableColumn identifier] isEqualToString:@"SERVICE_NAME"]) {
	
		NSString* value = [(NSDictionary*)[serviceData objectAtIndex:rowIndex] objectForKey:DB_SERVICE_NAME];
		result = value;
	
	} else if ([[aTableColumn identifier] isEqualToString:@"SERVICE_PROVIDER"]) { 
		
		NSString* value = [(NSDictionary*)[serviceData objectAtIndex:rowIndex] objectForKey:DB_SERVICE_PROVIDER];
		result = value;
	
	} else if ([[aTableColumn identifier] isEqualToString:@"SERVICE_SAT"]) {
	
		NSString* value = [(NSDictionary*)[serviceData objectAtIndex:rowIndex] objectForKey:DB_SERVICE_SAT];
		result = value;

	} else {
		result = nil;
	}
	
	return result;
}

/*
- (void)tableView:(NSTableView *)aTableView sortDescriptorsDidChange:(NSArray *)oldDescriptors {

	XDebugLog(@"sortDescriptorsDidChange = %@", oldDescriptors);
}
*/

-(void)tableView:(NSTableView*)tableView mouseDownInHeaderOfTableColumn:(NSTableColumn *)tableColumn {


	if ([[tableColumn identifier] isEqualToString:@"SERVICE_NAME"]) {
		
		if (columnSorted == 0)
			ascendingOrder = !ascendingOrder;

		columnSorted = 0;
		NSImage* image = (ascendingOrder ? [NSImage imageNamed:@"NSAscendingSortIndicator"] : [NSImage imageNamed:@"NSDescendingSortIndicator"]);
		[serviceTable setIndicatorImage:image inTableColumn:tableColumn];
		[serviceTable setIndicatorImage:nil inTableColumn:[tableView tableColumnWithIdentifier:@"SERVICE_PROVIDER"]];
		[serviceTable setIndicatorImage:nil inTableColumn:[tableView tableColumnWithIdentifier:@"SERVICE_SAT"]];

	} else if ([[tableColumn identifier] isEqualToString:@"SERVICE_PROVIDER"]) { 
		
		if (columnSorted == 1)
			ascendingOrder = !ascendingOrder;

		columnSorted = 1;
		NSImage* image = (ascendingOrder ? [NSImage imageNamed:@"NSAscendingSortIndicator"] : [NSImage imageNamed:@"NSDescendingSortIndicator"]);
		[serviceTable setIndicatorImage:image inTableColumn:tableColumn];
		[serviceTable setIndicatorImage:nil inTableColumn:[tableView tableColumnWithIdentifier:@"SERVICE_NAME"]];
		[serviceTable setIndicatorImage:nil inTableColumn:[tableView tableColumnWithIdentifier:@"SERVICE_SAT"]];

	} else if ([[tableColumn identifier] isEqualToString:@"SERVICE_SAT"]) {
		
		if (columnSorted == 2)
			ascendingOrder = !ascendingOrder;

		columnSorted = 2;
		NSImage* image = (ascendingOrder ? [NSImage imageNamed:@"NSAscendingSortIndicator"] : [NSImage imageNamed:@"NSDescendingSortIndicator"]);
		[serviceTable setIndicatorImage:image inTableColumn:tableColumn];
		[serviceTable setIndicatorImage:nil inTableColumn:[tableView tableColumnWithIdentifier:@"SERVICE_NAME"]];
		[serviceTable setIndicatorImage:nil inTableColumn:[tableView tableColumnWithIdentifier:@"SERVICE_PROVIDER"]];
		
	} 
	
	[self applySort];

}


#pragma mark outlineview selection

-(void) emptyTable {
	[serviceData removeAllObjects];
	[lbNumberOfServices setStringValue:[NSString stringWithFormat:@"Displaying %d services", [serviceData count]]];
}


- (void)populateAllSelection:(id)item {
	
	//reset searching data when selection changed
	initialData = nil;

	[self emptyTable];
	
	SidebarNode* node = (SidebarNode*)item;
	[btRemoveBouquet setEnabled:[node isEditable]];
	
	NSArray* allTVChannels = [dbManager allTV];
	serviceData = [NSMutableArray arrayWithArray:allTVChannels];
	
	//reapply search if any search in currently being done
	[self applySearch];
	
	//reapply sorting (will reload table)
	[self applySort];
}


- (void)hasAllSelection:(id)item {
	[NSThread detachNewThreadSelector:@selector(populateAllSelection:) toTarget:self withObject:item];
}


-(void)populateBouquetSelection:(id)item {
	
	//reset searching data when selection changed
	initialData = nil;
	
	[self emptyTable];
	
	SidebarNode* node = (SidebarNode*)item;
	[btRemoveBouquet setEnabled:[node isEditable]];
	
	XDebugLog(@"BouquetName = %@", node.nodeTitle );
	NSArray* channelsForBouquet = [dbManager servicesForBouquet:node.nodeTitle];
	serviceData = [NSMutableArray arrayWithArray:channelsForBouquet];
	
	//reapply search if any search in currently being done
	[self applySearch];
	
	//reapply sorting (will reload table)
	[self applySort];
	
}

- (void)hasBouquetSelection:(id)item {
	[NSThread detachNewThreadSelector:@selector(populateBouquetSelection:) toTarget:self withObject:item];
}

-(void)populateProviderSelection:(id)item {
	
	//reset searching data when selection changed
	initialData = nil;

	[self emptyTable];
	
	SidebarNode* node = (SidebarNode*)item;
	[btRemoveBouquet setEnabled:[node isEditable]];

	XDebugLog(@"providerID = %@, providerName= %@", [node.nodeData objectForKey:DB_SERVICE_REF],[node.nodeData objectForKey:DB_SERVICE_NAME] );
	NSArray* channelsForProvider = [dbManager servicesForProvider:[node.nodeData objectForKey:DB_SERVICE_NAME]];
	serviceData = [NSMutableArray arrayWithArray:channelsForProvider];
	
	//reapply search if any search in currently being done
	[self applySearch];
	
	//reapply sorting (will reload table)
	[self applySort];
	
}

- (void)hasProviderSelection:(id)item {
	[NSThread detachNewThreadSelector:@selector(populateProviderSelection:) toTarget:self withObject:item];
}



- (void)populateSatelliteSelection:(id)item {
	
	//reset searching data when selection changed
	initialData = nil;

	[self emptyTable];

	SidebarNode* node = (SidebarNode*)item;
	[btRemoveBouquet setEnabled:[node isEditable]];
	XDebugLog(@"hasSatelliteSelection on node %@", node.nodeTitle);
	
	NSArray* channelsForSatellite = [dbManager servicesForSatellite:node.nodeTitle];
	serviceData = [NSMutableArray arrayWithArray:channelsForSatellite];
	
	//reapply search if any search in currently being done
	[self applySearch];
		
	//reapply sorting (will reload table)
	[self applySort];
}

- (void)hasSatelliteSelection:(id)item {
	[NSThread detachNewThreadSelector:@selector(populateSatelliteSelection:) toTarget:self withObject:item];
}

#pragma mark add / remove user bouquet

- (IBAction)addUserBouquet:(id)sender {
	[sidebar addChild:@"New User Bouquet" withImage:[NSImage imageNamed:@"screen.png"] parentNode:bouquetNode action:@selector(hasNewSelection:) target:self editable:YES];	
}

- (IBAction)removeUserBouquet:(id)sender {
	[sidebar removeSelectedNode];
}


#pragma mark initialization

- (void)awakeFromNib {
	XDebugLog(@"ChannelsViewController awakeFromNib");
	serviceData = [[NSMutableArray alloc] init];
	[btRemoveBouquet setEnabled:NO];
	
    [[[serviceTable tableColumnWithIdentifier:@"SERVICE_NAME"] headerCell] setStringValue:@"Channel"];	
    [[[serviceTable tableColumnWithIdentifier:@"SERVICE_PROVIDER"] headerCell] setStringValue:@"Provider"];	
    [[[serviceTable tableColumnWithIdentifier:@"SERVICE_SAT"] headerCell] setStringValue:@"Satellite"];	
	
	columnSorted = 0;
	ascendingOrder = YES;
	[serviceTable setIndicatorImage:[NSImage imageNamed:@"NSAscendingSortIndicator"] inTableColumn:[serviceTable tableColumnWithIdentifier:@"SERVICE_NAME"]];
	
}


// -------------------------------------------------------------------------------
//	populateOutlineContents:inObject
//
//	This method is being called on a separate thread to avoid blocking the UI
//	a startup time.
// -------------------------------------------------------------------------------
- (void)populateOutlineContents:(id)inObject
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

	
	XDebugLog(@"populateOutlineContents - checking if files are read and parsed");
	while (dbManager.isParsing) {
		XDebugLog(@"populateOutlineContents - files are not ready yet, sleeping for a sec");
		[NSThread sleepForTimeInterval:1.0];
	}

	XDebugLog(@"populateOutlineContents - started");

	[NSAnimationContext beginGrouping];
	[[NSAnimationContext currentContext] setDuration:1.0];

	[[splitView animator] removeFromSuperview];
	[[splitView animator] setHidden:YES];
	[[waitPane animator] setHidden:NO];
	[sidebar startPopulatingContent];

	XDebugLog(@"populateOutlineContents - adding channels");
	
	SidebarNode* channelNode = [sidebar addRootFolder:@"CHANNELS"];
	[sidebar addChild:@"All" withImage:[NSImage imageNamed:@"bullet-red.png"] parentNode:channelNode action:@selector(hasAllSelection:) target:self editable:NO];	
	
	SidebarNode* satelliteNode = [sidebar addFolder:@"Satellite" withImage:[NSImage imageNamed:@"bullet-green.png"] parentNode:channelNode];	
	NSArray* satellites = [dbManager satellites];
	for (NSString* satName in satellites) {
		[sidebar addChild:satName withImage:[NSImage imageNamed:@"satellite.png"] parentNode:satelliteNode action:@selector(hasSatelliteSelection:) target:self editable:NO];	
	}
	
	XDebugLog(@"populateOutlineContents - adding providers");
	SidebarNode* providerNode = [sidebar addFolder:@"Provider" withImage:[NSImage imageNamed:@"bullet-yellow.png"] parentNode:channelNode];	

	XDebugLog(@"populateOutlineContents - reading providers");
	NSArray* providers = [dbManager providers];
	XDebugLog(@"populateOutlineContents - parsing providers");
	for (int i=0; i<[providers count]; i++) {
		
		NSDictionary* service = [providers objectAtIndex:i];
		[sidebar addChild:service withImage:[NSImage imageNamed:@"screen.png"] parentNode:providerNode action:@selector(hasProviderSelection:) target:self editable:NO idValue:DB_SERVICE_NAME];	
		
	}
	
	XDebugLog(@"populateOutlineContents - adding bouquets");
	bouquetNode = [sidebar addRootFolder:@"BOUQUETS"];
	
	XDebugLog(@"populateOutlineContents - reading bouquets");
	NSArray* bouquets = [dbManager bouquets];
	XDebugLog(@"populateOutlineContents - parsing bouquets");
	for (int i=0; i<[bouquets count]; i++) {
		
		NSDictionary* bouquet = [bouquets objectAtIndex:i];
		[sidebar addChild:bouquet withImage:[NSImage imageNamed:@"screen.png"] parentNode:bouquetNode action:@selector(hasBouquetSelection:) target:self editable:YES idValue:DB_BOUQUET_NAME];	

		//NSString* bouquet = [bouquets objectAtIndex:i];
		//[sidebar addChild:bouquet withImage:[NSImage imageNamed:@"screen.png"] parentNode:bouquetNode action:@selector(hasBouquetSelection:) target:self editable:YES];	
	}
	
	// remove the current selection
	[sidebar removeAnySelection];
	
	//quick hack to collapse the Satellite and Provider item
	//problem is to find the NSTreeNode from the NSTreeController or NSOutlineView or ???
	[sidebar.outlineView collapseItem:[sidebar.outlineView itemAtRow:2]];
	[sidebar.outlineView collapseItem:[sidebar.outlineView itemAtRow:3]];
	[sidebar.outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:1] byExtendingSelection:NO];
	[self hasAllSelection:[sidebar selectedNode]];
	
	[sidebar stopPopulatingContent];

	[[[tabView tabViewItemAtIndex:0].view animator] replaceSubview:waitPane with:splitView];
	//[[waitPane animator]  setHidden:YES];
	[[splitView animator] setHidden:NO];
	
	//try to solve NSTableColumnHeader display problems
	[splitView display];


	[NSAnimationContext endGrouping];
	
	XDebugLog(@"populateOutlineContents - end");
	
	[pool release];
}


#pragma mark search field Delegate

- (void)controlTextDidChange:(NSNotification *)aNotification {

	[self applySearch];
}

#pragma mark key handling

- (void)keyDown:(NSEvent *)theEvent {

	XDebugLog(@"Key down : %d ", [theEvent keyCode]);
}

@end
