//
//  DreamboxManager.h
//  iDream
//
//  Created by Sébastien Stormacq on 23/11/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@protocol DreamboxDelegate 

-(void)updateStatusWithMessage:(NSString*)msg;
-(void)indicateProgress:(BOOL)show;

@end

#define DB_INFO_TYPE		@"dbType"
#define DB_INFO_VERSION		@"dbVersion"
#define DB_INFO_MODEL		@"dbModel"
#define DB_SERVICE_REF		@"dbServiceReference"
#define DB_SERVICE_NAME		@"dbServiceName"
#define DB_SERVICE_PROVIDER @"dbServiceProvider"
#define DB_SERVICE_SAT      @"dbServiceSatellite"
#define DB_BOUQUET_NAME     @"dbBouquetName"
#define DB_BOUQUET_SERVIVES @"dbBouquetServices"
#define DB_BOUQUET_FILE     @"dbBouquetFileName"


@interface DreamboxManager : NSObject {
	
@private	
	NSString*	address;
	NSString*	username;
	NSString*   password;
	
	id<DreamboxDelegate> delegate;
	
	//a cache for documents retrieved from the dreambox webif
	//NSMapTable* cache;
	NSMutableDictionary* cache;

	//content of lamedb
	NSDictionary* tvServices;
	NSDictionary* radioServices;
	NSDictionary* transponders;
	NSDictionary* providers;
	NSDictionary* allSatellites;		//all satellites known from the dreambox (comming from satellites.xml)
	NSDictionary* satellites;			//all satellites actually in use, i.e. referenced from lamedb
	NSDictionary* userBouquets;			//all user bouquets key = bouquet name  value0 is file name value1 is an NSArray of services (NSDictionary)
	
	BOOL isParsing;
}


@property (readonly) NSString* address;
@property (readonly) NSString* username;
@property (readonly) NSString* password;
@property (assign)   id<DreamboxDelegate> delegate;
@property (readonly) BOOL isParsing;

+(BOOL)validateAddress:(NSString*) address error:(NSString**)errorMsg;
+(DreamboxManager*)dreamboxManagerWithDictionary:(NSDictionary*)initData;

-(NSDictionary*)dbInfo;
-(NSArray*)bouquets;
-(NSArray*)providers;
-(NSArray*)allTV;
-(NSArray*)satellites;
-(NSArray*)servicesForProvider:(NSString*)providerName;
-(NSArray*)servicesForBouquet:(NSString*)bouquetName;
-(NSArray*)servicesForSatellite:(NSString*)satelliteName;

@end
