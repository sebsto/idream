//
//  AboutWindowController.h
//  iDream
//
//  Created by Sébastien Stormacq on 25/11/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface AboutWindowController : NSWindowController {
	
	IBOutlet NSTextField* versionNumber;
	IBOutlet NSTextField* buildNumber;

}

- (void)showAboutBox;

@end
