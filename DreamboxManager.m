//
//  DreamboxManager.m
//  iDream
//
//  Created by Sébastien Stormacq on 23/11/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import "DreamboxManager.h"

#import "log.h"

#define DB_URL_ABOUT		@"/web/about"
#define DB_URL_BOUQUET		@"/web/getservices"
//#define DB_URL_TV_PROVIDERS @"/web/getservices?sRef=1:7:1:0:0:0:0:0:0:0:(type == 1) || (type == 17) || (type == 195) || (type == 25) FROM PROVIDERS ORDER BY name"
//#define DB_URL_ALL_TV		@"/web/getservices?sRef=1:7:1:0:0:0:0:0:0:0:(type == 1) || (type == 17) || (type == 195) || (type == 25) ORDER BY name"
//#define DB_URL_SERVICES_FOR_PROVIDER @"/web/getservices?sRef=1:7:0:0:0:0:0:0:0:0:(provider==\"%@\") && (type == 1) || (type == 17) || (type == 195) || (type == 25) ORDER BY name:%@"


#define MSG_RETRIEVE_DB_INFO			@"Retrieving and Parsing Dreambox information ...."
#define MSG_RETRIEVE_SAT				@"Retrieving and Parsing Satellite information ...."
#define MSG_RETRIEVE_PROVIDER			@"Retrieving and Parsing Provider information ...."
#define MSG_RETRIEVE_BOUQUET			@"Retrieving and Parsing Bouquet information ...."
#define MSG_PARSING						@"Reading information ..."
#define MSG_COMMUNICATION_ERROR			@"Can not communicate with the dreambox at %@ : %@ "
#define MSG_READY						@"Ready"
#define MSG_FTP_SAVE_ERROR				@"Can not save dreambox file to %@ : %@"
#define MSG_GENERIC_ERROR_WITH_SUGG		@"%@\nCause : %@\nSuggestion : %@"
#define MSG_GENERIC_ERROR				@"%@\nCause : %@"

//the one and only DB manager
static DreamboxManager* singleton = nil;

@interface DreamboxManager (Private)

-(void)retrieveDreamboxFiles;
-(NSString*)applicationSupportDirectory;
-(void)reportFatalError:(NSString*)title error:(NSError*)err;
-(void)reportFatalError:(NSString*)title message:(NSString*)msg;
-(void)parseLameDB;
-(void)parseSatellites;
-(void)parseUserBouquetFile:(NSString*)name;
-(NSArray*)sortArray:(NSArray*)source forKey:(NSString*)dictKey;
-(NSString*)transponderFromServiceID:(NSString*)serviceID;
-(NSString*)satelliteNameFromTransponderID:(NSString*)transpondeurID;
-(NSDictionary*)satelliteFromTransponderID:(NSString*)transpondeurID;
-(NSDictionary*)serviceByServiceID:(NSString*)serviceID;
-(NSString*)transformLameDBServiceIDToBouquetServiceID:(NSString*)serviceID;

@end


@implementation DreamboxManager


@synthesize address, username, password, delegate, isParsing;

#pragma mark Initialization

// -------------------------------------------------------------------------------
//	initWithAddress:userName:andPassword
// -------------------------------------------------------------------------------
- (id)initWithAddress:(NSString*)addr userName:(NSString*)user andPassword:(NSString*)pwd {
	
	self = [super init];
	
	if (self) {
		
		address  = addr;
		username = user;
		password = pwd;
		
		//cache = [NSMapTable mapTableWithWeakToWeakObjects];
		cache = [[NSMutableDictionary alloc] init];
	}

	//async reading and parsing the file
	//[NSThread detachNewThreadSelector:@selector(retrieveDreamboxFiles) toTarget:self withObject:nil];
	[self performSelectorOnMainThread:@selector(retrieveDreamboxFiles) withObject:nil waitUntilDone:NO];
	
	return self;
}


// -------------------------------------------------------------------------------
//	dreamboxManager:
//
//  return the singleton already initialized  
// -------------------------------------------------------------------------------

+(DreamboxManager*)dreamboxManager {

	assert (singleton != nil); //programmer error
	return singleton;

}


// -------------------------------------------------------------------------------
//	dreamboxManagerWithDictionary:
//
//  return the singleton initialized with the values provided in the dictionary  
// -------------------------------------------------------------------------------

+(DreamboxManager*)dreamboxManagerWithDictionary:(NSDictionary*)initData {

	if (!singleton) {
	
		singleton = [[DreamboxManager alloc] initWithAddress:[initData objectForKey:@"address"] 
													 userName:[initData objectForKey:@"username"]
												     andPassword:[initData objectForKey:@"password"]];
	}
	
	return singleton;
}

// -------------------------------------------------------------------------------
//	updateStatus:
//
//  Notify the delegate about what we are doing  
// -------------------------------------------------------------------------------
-(void)updateStatusWithMessage:(NSString*)msg {
	
	[delegate updateStatusWithMessage:msg];
}

#pragma mark HTTP Communication

// -------------------------------------------------------------------------------
//	dbInfo:
//
//  return data from http://address/web/about  
// -------------------------------------------------------------------------------
-(NSDictionary*)dbInfo {
	
	[delegate indicateProgress:YES];
	
	
	NSString* sURL = [NSString stringWithFormat:@"http://%@%@", address, DB_URL_ABOUT];
	sURL = [sURL stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];;
	NSURL* url = [NSURL URLWithString:sURL];
	XDebugLog(@"dbInfo URL = %@", url);
	
	NSMutableDictionary* result;
	
	//check cache first
	if (result = [cache objectForKey:url]) {
		XDebugLog(@"Getting dbInfo from cache");
		[delegate indicateProgress:NO];
		return result;
	} else 
		result = [[NSMutableDictionary alloc] init];
	
	NSError* error;

	[self updateStatusWithMessage:MSG_RETRIEVE_DB_INFO];	 
	NSXMLDocument* xmlDoc = [[NSXMLDocument alloc] initWithContentsOfURL:url options:NSXMLDocumentTidyXML error: &error] ;
	XDebugLog(@"dbInfo = %@", xmlDoc);

	if (xmlDoc) {
		[self updateStatusWithMessage:MSG_PARSING];	 
		
		//TODO : improve error handling
		NSArray* node = [xmlDoc nodesForXPath:@"/e2abouts/e2about/e2imageversion" error:&error];
		[result setObject:[(NSXMLNode*)[node objectAtIndex:0] stringValue] forKey:DB_INFO_VERSION];

		node = [xmlDoc nodesForXPath:@"/e2abouts/e2about/e2model" error:&error];
		[result setObject:[[(NSXMLNode*)[node objectAtIndex:0] stringValue] uppercaseString] forKey:DB_INFO_TYPE];
		[result setObject:[(NSXMLNode*)[node objectAtIndex:0] stringValue] forKey:DB_INFO_MODEL];
		
		[cache setObject:result forKey:url];

		[self updateStatusWithMessage:MSG_READY];	 
	}
	else {
		//either the connection failed, either it returned an non XML document
		XLog(@"Error - xmlDoc is nil !");
		[self updateStatusWithMessage:[NSString stringWithFormat:MSG_COMMUNICATION_ERROR, sURL, [error localizedDescription]]];	 
	}

	[delegate indicateProgress:NO];
	

	return result;

}


#pragma mark Data Sources for GUI

// -------------------------------------------------------------------------------
//	providers:
//
//  return data from lamedb
// -------------------------------------------------------------------------------
-(NSArray*)providers {

	return [self sortArray:[providers allValues] forKey:DB_SERVICE_NAME];
}


// -------------------------------------------------------------------------------
//	bouquets:
//
//  return data from http://address/web/getservices
// -------------------------------------------------------------------------------
-(NSArray*)bouquets {
	
	//return [[userBouquets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
	//return [self sortArray:[userBouquets allValues] forKey:DB_BOUQUET_FILE];

	//TODO - Sorting by file is incorrect, should use the order provided by /etc/enigma2/bouquets.tv
	
	NSArray* userBouquetValues = [userBouquets allValues];
	//XDebugLog(@"Before Sort -> userBouquets = %@", userBouquetValues);
	userBouquetValues = [self sortArray:userBouquetValues forKey:DB_BOUQUET_FILE];
	//XDebugLog(@"After Sort -> userBouquets = %@", userBouquetValues);

	return userBouquetValues;
	
}

// -------------------------------------------------------------------------------
//	allTV:
//
//  return data from lamedb
// -------------------------------------------------------------------------------
-(NSArray*)allTV {
	
	return [tvServices allValues];
}

// -------------------------------------------------------------------------------
//	satellites:
//
//  return data from lamedb
// -------------------------------------------------------------------------------
-(NSArray*)satellites {
	
	return [satellites allValues];
}

// -------------------------------------------------------------------------------
//	validateAddress:
//
//  return YES when http://address/web/about returns a valid DM about XML document 
// -------------------------------------------------------------------------------
+(BOOL)validateAddress:(NSString*)address error:(NSString**) errorMsg {
	
	//TODO 1 : use NSError instead of errorMsg
	//TODO 2 : should I cache XML documents ?
	
	NSString* sURL = [NSString stringWithFormat:@"http://%@%@", address, DB_URL_ABOUT];
	NSURL* url = [NSURL URLWithString:sURL];
	
	NSError* error;
	
	NSXMLDocument* xmlDoc = [[NSXMLDocument alloc] initWithContentsOfURL:url options:NSXMLDocumentTidyXML error: &error] ;
	
	BOOL result = NO;
	
	if (xmlDoc) {
		XDebugLog(@"%@", xmlDoc);
		
		NSArray* node = [xmlDoc nodesForXPath:@"/e2abouts/e2about" error:&error];
		if ([node count] > 0) 
			result = YES;
		else {
			//an xml doc is returned but it does not look like an enigma 2 doc
			*errorMsg = [NSString stringWithFormat:@"The server at %@ does not appear to be a Dreambox running Enigma2.", sURL];
		}
	}
	else {
		
		//either the connection failed, either it returned an non XML document
		*errorMsg = [NSString stringWithFormat:@"Can not find a Dreambox running Enigma2 at %@", sURL];
	}
	
	return result;
}

#pragma mark fatal error reporting

// -------------------------------------------------------------------------------
//	reportFatalError:message
// -------------------------------------------------------------------------------
-(void)reportFatalError:(NSString*)title error:(NSError*)err {
	
	if ([err localizedRecoverySuggestion]) 
		NSRunAlertPanel(NSLocalizedString(title, title),
						[NSString stringWithFormat:MSG_GENERIC_ERROR_WITH_SUGG, [err localizedDescription], [err localizedFailureReason], [err localizedRecoverySuggestion]],
						 @"Cancel", nil, nil);
	else
		NSRunAlertPanel(NSLocalizedString(title, title),
						[NSString stringWithFormat:MSG_GENERIC_ERROR, [err localizedDescription], [err localizedFailureReason]],
						@"Cancel", nil, nil);
	[[NSApplication sharedApplication] terminate:self];
}


// -------------------------------------------------------------------------------
//	reportFatalError:message
// -------------------------------------------------------------------------------
-(void)reportFatalError:(NSString*)title message:(NSString*)msg {
	
	NSRunAlertPanel(NSLocalizedString(title, title), NSLocalizedString(msg, msg), @"Quit", nil, nil);
	[[NSApplication sharedApplication] terminate:self];
}

#pragma mark FTP Communications

// -------------------------------------------------------------------------------
//	sortArray:
//
//  returns the array sorted on NSDictionary 's dictKey
//  creates it when it does not exists
// -------------------------------------------------------------------------------
-(NSArray*)sortArray:(NSArray*)source forKey:(NSString*)dictKey {

	// Note the use of the localizedCaseInsensitiveCompare: selector
	NSSortDescriptor *descriptor =
		[[NSSortDescriptor alloc] initWithKey:dictKey ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];

	NSArray *descriptors = [NSArray arrayWithObjects:descriptor, nil];
	return [source sortedArrayUsingDescriptors:descriptors];
}

// -------------------------------------------------------------------------------
//	applicationSupportDirectory:
//
//  returns the Application Support/iDream directory
//  creates it when it does not exists
// -------------------------------------------------------------------------------
-(NSString*)applicationSupportDirectory {

	NSFileManager* fileManager = [NSFileManager defaultManager];
	NSError* error;
	
	//0.  Check and create Application Support/iDream if required
	NSString* applicationDirectoryPath;
	NSArray* paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
	if ([paths count] < 1) {
		
		XLog(@"Application Support directory does not exist");
		[self reportFatalError:@"Your \"Application Directory\" folder does not exist."
					   message:@"Your home folder appears to be missing its ""Application Support"" folder, which is required by many parts of Mac OS X. You should restore this folder from backups or create a new account for yourself and move your personal files over to it."];
		
	} else {
		
		applicationDirectoryPath = [paths objectAtIndex:0];
		applicationDirectoryPath = [applicationDirectoryPath stringByAppendingPathComponent:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"]];
		
		if (![fileManager fileExistsAtPath:applicationDirectoryPath]) {
			
			XDebugLog(@"Application Support/iDream does not exist - creating it");
			if (![fileManager createDirectoryAtPath:applicationDirectoryPath withIntermediateDirectories:YES attributes:nil error:&error]) {
				XLog(@"Can not create %@", applicationDirectoryPath);
				[self reportFatalError:@"Can not create iDream directory" error:error];
			}
		}
	}
	
	return applicationDirectoryPath;
}

// -------------------------------------------------------------------------------
//	retrieveFile:atDirectory
//
//  Generic file retrieval method for the DB
//	it always use ftp://username:password@address URL
// -------------------------------------------------------------------------------
-(void)retrieveFile:(NSString*)file atDirectory:(NSString*)dir {
	
	NSError* error;
	
	NSString* sURL = [NSString stringWithFormat:@"ftp://%@:%@@%@%@/%@", username, password, address, dir, file];
	NSURL* url = [NSURL URLWithString:sURL];
	
	XDebugLog(@"Getting %@ at %@", file, sURL);
	//NSString* fileContent = [NSString stringWithContentsOfURL:url encoding:NSASCIIStringEncoding  error:&error];
	NSData* fileContent = [NSData dataWithContentsOfURL:url];
	
	if (fileContent) {
		
		//save to Application Support directory
		NSString* applicationSupportDirectorypath = [self applicationSupportDirectory];
		NSString* destinationPath = [applicationSupportDirectorypath stringByAppendingPathComponent:file];
		
		if (![fileContent writeToFile:destinationPath options:NSAtomicWrite  error:&error]) {
			XLog(@"Can not save %@ to %@ : %@", file, destinationPath, [error localizedDescription]);
			[self reportFatalError:[NSString stringWithFormat:@"Can not write file %@", destinationPath] error:error];
		} else {
			XDebugLog(@"Succeded !  Saved %@ to Application Support : %@", file, destinationPath);
		}
		
	} else {
		
		XLog(@"Can not get %@ : %@", file, [error localizedDescription]);
		[self reportFatalError:[NSString stringWithFormat:@"Can not read file %@", sURL] error:error];
	}
}

// -------------------------------------------------------------------------------
//	getUserBouquetsFileNames:
//
// uses curl to get an /etc/enigma2 directory listing, then 
// parse it to get the userbouquets file names
// -------------------------------------------------------------------------------
-(NSArray*)getUserBouquetsFileNames {
	
	NSMutableArray* result = [[NSMutableArray alloc] init];
	
	NSString* cmd = @"/usr/bin/curl";
	NSString* args = [NSString stringWithFormat:@"ftp://%@:%@@%@/etc/enigma2/", username, password, address];
	
	NSTask *task;
    task = [[NSTask alloc] init];
    [task setLaunchPath: cmd];
	
    NSArray *arguments;
    arguments = [args componentsSeparatedByString:@" "];
    [task setArguments: arguments];
	
    NSPipe *pipe;
    pipe = [NSPipe pipe];
    [task setStandardOutput: pipe];
	
    NSFileHandle *file;
    file = [pipe fileHandleForReading];
	
    [task launch];
	
    NSData *data;
    data = [file readDataToEndOfFile];
	
    NSString *string;
    string = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    
	XDebugLog(@"Raw Lines = \n%@", string);
	
	if (!string || [[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0 ) {
		XLog(@"Can not retrieve user bouqet from dreambox : %@", args);
		[self reportFatalError:[NSString stringWithFormat:@"Can not retrieve user bouquet from %@", args] message:@"Are you sure your username, password and dreambox address are correct ?"];
		return nil;
	}
	
	NSArray* lines = [string componentsSeparatedByString:@"\n"];
	for(NSString* line in lines) {
	
		NSRange range;
		if ((range = [line rangeOfString:@"userbouquet"]).location != NSNotFound) {
			
			[result addObject:[line substringFromIndex:range.location]];
		}
	}
	
	XDebugLog(@"Parsed Lines = %@", result);
	
	return result;
	
}

// -------------------------------------------------------------------------------
//	retrieveDreamboxFiles:
//
// uses curl and Cocoa to get /etc/satellites.xml and /etc/enigma2/lamedb
// /etc/enigma2/userbouquet*
// -------------------------------------------------------------------------------
-(void)retrieveDreamboxFiles {
	
#define LOOP_RUNTIME 0.1
	
	NSRunLoop* loop = [NSRunLoop currentRunLoop];
	
	[delegate indicateProgress:YES];
	
	isParsing = YES;

	//allow the run loop to fetch events
	[loop runUntilDate:[NSDate dateWithTimeIntervalSinceNow:LOOP_RUNTIME]];

	//1. get SATELLITES.XML
	[self updateStatusWithMessage:MSG_RETRIEVE_SAT];	
	[self retrieveFile:@"satellites.xml" atDirectory:@"/etc"];
	[self parseSatellites];
	
	//allow the run loop to fetch events
	[loop runUntilDate:[NSDate dateWithTimeIntervalSinceNow:LOOP_RUNTIME]];
	
	//2. get LAMEDB
	[self updateStatusWithMessage:MSG_RETRIEVE_PROVIDER];	
	[self retrieveFile:@"lamedb" atDirectory:@"/etc/enigma2"];
	[self parseLameDB];
	//XDebugLog(@"providers = %@", providers);

	//allow the run loop to fetch events
	[loop runUntilDate:[NSDate dateWithTimeIntervalSinceNow:LOOP_RUNTIME]];
	
	//3. get user bouquets
	[self updateStatusWithMessage:MSG_RETRIEVE_BOUQUET];	
	for (NSString* name in [self getUserBouquetsFileNames]) {
		[self retrieveFile:name atDirectory:@"/etc/enigma2"];
		[self parseUserBouquetFile:name];
	}
	
	//allow the run loop to fetch events
	[loop runUntilDate:[NSDate dateWithTimeIntervalSinceNow:LOOP_RUNTIME]];
	
	//TODO
	//4. get order of user bouquets

	isParsing = NO;

	[self updateStatusWithMessage:MSG_READY];
	[delegate indicateProgress:NO];
}

#pragma mark Parsing


-(NSString*)removeLeadingZeroFromString:(NSString*)original {

	NSMutableString* result = [[NSMutableString alloc] init];
	
	BOOL leading = YES;
	for (int i =0 ; i < [original length]; i++) {
		
		unichar c = [original characterAtIndex:i];
		if (! (c == '0' && leading)) {
			leading = NO;
			[result appendFormat:@"%c", c];
		} //else skip the char
	}
	
	return result;
	
}

// -------------------------------------------------------------------------------
//  transformLameDBServiceIDToBouquetServiceID
//
// LAMEDB  = 23f3:00c00000:0452:0001:25:0
// BOUQUET = 1:0:19:23F3:452:1:C00000:0:0:0:
//
// 1:0:19:23F3:452:1:C00000:0:0:0:
// 1:0:19:23F3:452:1:C00000:0:0:0:
// -------------------------------------------------------------------------------
-(NSString*)transformLameDBServiceIDToBouquetServiceID:(NSString*)serviceID {
	
	NSArray* details = [serviceID componentsSeparatedByString:@":"];
	
	NSMutableArray* transformed = [[NSMutableArray alloc] initWithCapacity:10];

	[transformed insertObject:@"1" atIndex:0];
	[transformed insertObject:@"0" atIndex:1];
	[transformed insertObject:[NSString stringWithFormat:@"%X", [[details objectAtIndex:4] integerValue]] atIndex:2]; //hexa value of field #4
	[transformed insertObject:[[details objectAtIndex:0] uppercaseString] atIndex:3];
	[transformed insertObject:[self removeLeadingZeroFromString:[details objectAtIndex:2]] atIndex:4]; //remove leading 0
	//[transformed insertObject:@"x" atIndex:4];
	[transformed insertObject:@"1" atIndex:5];
	[transformed insertObject:[self removeLeadingZeroFromString:[details objectAtIndex:1]] atIndex:6]; //remove leading 0
	//[transformed insertObject:@"x" atIndex:6];
	[transformed insertObject:@"0" atIndex:7];
	[transformed insertObject:@"0" atIndex:8];
	[transformed insertObject:@"0" atIndex:9];
	
	NSString* result = [transformed componentsJoinedByString:@":"];
	result = [result stringByAppendingString:@":"];
	
	//XDebugLog(@"ServiceID Transformation : %@ = %@", serviceID, result);
	
	return [result uppercaseString];
}

// -------------------------------------------------------------------------------
//  serviceByServiceID
//
// -------------------------------------------------------------------------------
-(NSDictionary*)serviceByServiceID:(NSString*)serviceID {
	
	return [tvServices objectForKey:serviceID];
}

// -------------------------------------------------------------------------------
//  servicesForBouquet
//
// -------------------------------------------------------------------------------
-(NSArray*)servicesForBouquet:(NSString*)bouquetName {
	
	NSArray* result;
	
	result = [[userBouquets objectForKey:bouquetName] objectForKey:DB_BOUQUET_SERVIVES];
	
	return result;
}

// -------------------------------------------------------------------------------
//  servicesForSatellite
//
// -------------------------------------------------------------------------------
-(NSArray*)servicesForSatellite:(NSString*)satelliteName {
	NSArray* result;
	
	NSString* predicateString = [NSString stringWithFormat:@"(%@ == '%@')", DB_SERVICE_SAT, satelliteName];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateString];
	result = [[tvServices allValues] filteredArrayUsingPredicate:predicate];

	//XDebugLog(@"tvServices = %@", tvServices);
	//XDebugLog(@"predicateString = %@", [predicate predicateFormat]);
	//XDebugLog(@"filtered array = %@", result);
	
	return result;
}

// -------------------------------------------------------------------------------
//  servicesForProvider
//
// -------------------------------------------------------------------------------
-(NSArray*)servicesForProvider:(NSString*)providerName {
	
	NSArray* result;
	
	NSString* predicateString = [NSString stringWithFormat:@"(%@ == '%@')", DB_SERVICE_PROVIDER, providerName];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateString];
	result = [[tvServices allValues] filteredArrayUsingPredicate:predicate];
	
	//XDebugLog(@"tvServices = %@", tvServices);
	//XDebugLog(@"predicateString = %@", predicateString);
	//XDebugLog(@"filtered array = %@", result);
	
	/*
	NSDictionary* dict1 = [NSDictionary dictionaryWithObjectsAndKeys:@"OBJ10", @"KEY1", @"OBJ20", @"KEY2", nil];
	NSDictionary* dict2 = [NSDictionary dictionaryWithObjectsAndKeys:@"OBJ11", @"KEY1", @"OBJ21", @"KEY2", nil];
	NSDictionary* dict3 = [NSDictionary dictionaryWithObjectsAndKeys:@"OBJ12", @"KEY1", @"OBJ22", @"KEY2", nil];
	NSArray *data = [NSArray arrayWithObjects: dict1, dict2, dict3, nil];    
	NSArray *filtered = [data filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(KEY1 == %@)", @"OBJ10"]];

	XDebugLog(@"dict1 = %@", dict1);
	XDebugLog(@"data = %@", data);
	XDebugLog(@"filtered = %@", filtered);
	*/
	
	return result;
}

// -------------------------------------------------------------------------------
//  satelliteNameFromTransponderID
//
//  0082acce:0578:013e               13e = transpondeurID
//  s 11470760:27500000:1:4:130:2:0  130 = satelitteID
// -------------------------------------------------------------------------------
-(NSString*)satelliteNameFromTransponderID:(NSString*)transponderID {

	//XDebugLog(@"xxxxxxxxxxxxxxxxxxxxxx");
	NSString* trans = [transponders objectForKey:transponderID];
	//XDebugLog(@"transponder : %@ -- %@", transponderID, trans);
	
	NSString* satID   = [[trans componentsSeparatedByString:@":"] objectAtIndex:4];
	//XDebugLog(@"satID = %@", satID);
	
	NSString* result = [allSatellites objectForKey:satID];
	//XDebugLog(@"result = %@", result);
	//XDebugLog(@"xxxxxxxxxxxxxxxxxxxxxx");
	
	return result;

}

// -------------------------------------------------------------------------------
//  satelliteFromTransponderID
//
//  0082acce:0578:013e               13e = transpondeurID
//  s 11470760:27500000:1:4:130:2:0  130 = satelitteID
// -------------------------------------------------------------------------------
-(NSDictionary*)satelliteFromTransponderID:(NSString*)transponderID {
	
	//XDebugLog(@"xxxxxxxxxxxxxxxxxxxxxx");
	NSString* trans = [transponders objectForKey:transponderID];
	//XDebugLog(@"transponder : %@ -- %@", transponderID, trans);
	
	NSString* satID   = [[trans componentsSeparatedByString:@":"] objectAtIndex:4];
	//XDebugLog(@"satID = %@", satID);
	
	NSDictionary* result = [NSDictionary dictionaryWithObjectsAndKeys:[allSatellites objectForKey:satID], satID, nil];
	//XDebugLog(@"result = %@", result);
	//XDebugLog(@"xxxxxxxxxxxxxxxxxxxxxx");
	
	return result;
	
}

// -------------------------------------------------------------------------------
//  transponderFromServiceID
//
//  12cf:00820000:2e7c:013e:0:0  field #4 is transponder ID
//  119 test 1 E
//  p:CYFRA +
// -------------------------------------------------------------------------------
-(NSString*)transponderFromServiceID:(NSString*)serviceID {

	NSArray* details = [serviceID componentsSeparatedByString:@":"];
	return [details objectAtIndex:3];
}


// -------------------------------------------------------------------------------
//  isTVService
//
//  0e2b:00820000:0708:00c8:1:0
//  :1: is the service type
//
//  TV Channels are 1 || 17 || 195 || 25
// -------------------------------------------------------------------------------
-(BOOL)isTVService:(NSString*)serviceID {

	NSArray* details = [serviceID componentsSeparatedByString:@":"];
	NSString* serviceType = [details objectAtIndex:4];
	
	return [serviceType isEqualToString:@"1"] ||
			[serviceType isEqualToString:@"195"] ||
			[serviceType isEqualToString:@"17"] ||
		    [serviceType isEqualToString:@"25"];
}

// -------------------------------------------------------------------------------
//  isRadioService
//
//  0e2b:00820000:0708:00c8:1:0
//  :1: is the service type
//
//  Radio Channels are 2
// -------------------------------------------------------------------------------
-(BOOL)isRadioService:(NSString*)serviceID {
	
	NSArray* details = [serviceID componentsSeparatedByString:@":"];
	NSString* serviceType = [details objectAtIndex:4];
	
	return [serviceType isEqualToString:@"2"];
}


// -------------------------------------------------------------------------------
//  addProvider
//
// -------------------------------------------------------------------------------
-(NSString*)addProvider:(NSString*)providerString {

	NSRange start = [providerString rangeOfString:@"P:"];
	NSRange end   = [providerString rangeOfString:@","];

	//XDebugLog(@"start = %d, %d", start.location, start.length);
	//XDebugLog(@"end = %d, %d", end.location, end.length);
	//XDebugLog(@"range = %d, %d", start.location + start.length, end.location - start.location - start.length);

	NSString* providerName;
	if (end.location == NSNotFound) {
		
		providerName = [providerString substringFromIndex:start.length];
		
	} else {
		
		NSRange providerRange = {start.location + start.length, end.location - start.location - start.length};
		providerName = [providerString substringWithRange:providerRange];
		
	}
	
	
	if (![@"" isEqualToString:providerName]) {
	
		//XDebugLog(@"%@", providerString);
		//XDebugLog(@"%@\n", providerName);
		NSDictionary* prov = [NSDictionary dictionaryWithObjectsAndKeys:providerString, DB_SERVICE_REF, providerName, DB_SERVICE_NAME, nil];
		[(NSMutableDictionary*)providers setObject:prov forKey:providerName];
	}
	
	return providerName;
}

// -------------------------------------------------------------------------------
//	parseLameDB
//
//  parse lamedb file and setup the following arrays : 
//   - tvServices (array of NSDictionary* - SERVICE_REF / SERVICE_NAME) 
//   - radioServices (array of NSDictionary* - SERVICE_REF / SERVICE_NAME) 
//   - transponders (array of NSDictionar* - TRANS_REF / TRANS_INFO)
//   - providers (NSDictionary* - PROVIDER_NAME / NSDictionary* (SERVICE_REF, SERVICE_NAME) ) 
//
//  lamedb is read from Application Support/iDream directory and assume to be present
// -------------------------------------------------------------------------------
-(void)parseLameDB {

	NSError* error;
	
	tvServices    = [[NSMutableDictionary alloc] init];
	radioServices = [[NSMutableDictionary alloc] init];
	transponders  = [[NSMutableDictionary alloc] init];
	providers     = [[NSMutableDictionary alloc] init];
	satellites    = [[NSMutableDictionary alloc] init];
	
	NSString* applicationSupportDirectoryPath = [self applicationSupportDirectory];
	NSString* lamedbPath = [applicationSupportDirectoryPath stringByAppendingPathComponent:@"lamedb"];
	
	NSString* lamedb = [[NSString stringWithContentsOfFile:lamedbPath encoding:NSUTF8StringEncoding error:&error] uppercaseString];
	
	if (!lamedb) {
		
		XLog(@"Can not read lamedb from Application Support directory : %@", lamedbPath);
		[self reportFatalError:[NSString stringWithFormat:@"Can not read file %@", lamedbPath] error:error];
		return;
	}

	NSArray* lines = [lamedb componentsSeparatedByString:@"\n"];
	NSUInteger cursor = 0;

	NSString* line = [lines objectAtIndex:cursor];

	////////
	//
	// First read the transponder part
	//
	////////
	while (! [@"TRANSPONDERS" isEqualToString:line] ) {
		line = [lines objectAtIndex:++cursor];
	}
		
	//skip the transponder line itself
	cursor++;
	
	//read all transponders
	BOOL noMoreTransponder = NO;
	while (! noMoreTransponder) {
		
		//read first line of transponder
		NSString* line1 = [lines objectAtIndex:cursor++];
		
		//XDebugLog(@"line 1 : %@", line1);

		if ( [@"END" isEqualToString:line1]) {
			noMoreTransponder = YES;
	    } else {
			
			//use only the last element (= transpondeur ID), the rest is useless for us.
			//0082acce:0578:013e               13e = transpondeurID
			NSArray* details = [line1 componentsSeparatedByString:@":"];
			line1 = [details objectAtIndex:2];

			//read second line of transponder
			line = [lines objectAtIndex:cursor++];
			//XDebugLog(@"line 2 : %@", line);

			//store the transponder
			//NSMutableDictionary* newTransponder = [NSMutableDictionary dictionaryWithObjectsAndKeys:line1, DB_TRANSPONDER_REF, line, DB_TRANSPONDER_INFO, nil];
			[(NSMutableDictionary*)transponders setObject:line forKey:line1];
			
			//add the satellite in our list of satellite being used
			NSDictionary* sat = [self satelliteFromTransponderID:line1];
			[(NSMutableDictionary*)satellites addEntriesFromDictionary:sat];
			//XDebugLog(@"---------- adding satellite = %@", sat);
			 
			//skip the / line
			cursor++;
		}
	}

	XDebugLog(@"End transponder Parsing : %d items", [transponders count]);

	//skip "services" line
	cursor++;
	
	//////////
	//
	// Then read the services (each one is using three lines
	//
	////////
	BOOL noMoreServices = NO;
	while (! noMoreServices) {
	
		//read first line of service
		NSString* line1 = [lines objectAtIndex:cursor++];
		//XDebugLog(@"line 1 : %@", line1);
		
		if ( [@"END" isEqualToString:line1]) {
			noMoreServices = YES;
		} else {
		
			//read second line of service
			NSString* line2 = [lines objectAtIndex:cursor++];
			//XDebugLog(@"line 2 : %@", line2);

			//read third line of service
			line = [lines objectAtIndex:cursor++];
			//XDebugLog(@"line 3 : %@", line);
			
			//is it a radio or TV ?
			if ([self isTVService:line1] &&
				[[line2 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0 &&
				![line2 isEqualToString:@"."] ) {
				
				//add the provider if required
				line = [self addProvider:line];
				
				NSString* trans = [self transponderFromServiceID:line1];
				NSString* satName = [self satelliteNameFromTransponderID:trans];
				
				line1 = [self transformLameDBServiceIDToBouquetServiceID:line1];
				
				NSMutableDictionary* newService = [NSMutableDictionary dictionaryWithObjectsAndKeys:line1, DB_SERVICE_REF, line2, DB_SERVICE_NAME, line, DB_SERVICE_PROVIDER, satName, DB_SERVICE_SAT, nil];
				
				//XDebugLog(@"newService = %@", newService);
				//[(NSMutableArray*)tvServices addObject:newService];
				[(NSMutableDictionary*)tvServices setObject:newService forKey:line1];
				
			} else if ([self isRadioService:line1] &&
					   [[line2 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0 &&
					   ![line2 isEqualToString:@"."]) {
				
				//add the provider if required
				[self addProvider:line];

				NSMutableDictionary* newService = [NSMutableDictionary dictionaryWithObjectsAndKeys:line1, DB_SERVICE_REF, line2, DB_SERVICE_NAME, line, DB_SERVICE_PROVIDER, nil];
				//[(NSMutableArray*)radioServices addObject:newService];
				[(NSMutableDictionary*)radioServices setObject:newService forKey:line1];
			}
			
		}
			
	}
	
	XDebugLog(@"End service Parsing : %d TV items, %d Radio items, %d providers and %d transponders", [tvServices count], [radioServices count], [providers count], [transponders count]);
	
}

// -------------------------------------------------------------------------------
//	parseSatellites
//
//  parse satellites.xml file and setup the following arrays : 
//   - satellites (NSDictionary* - satellite position / satellite ename) 
//   
//
//  satellites.xml is read from Application Support/iDream directory and assume to be present
// -------------------------------------------------------------------------------
-(void)parseSatellites {
	NSError* error;
	
	allSatellites = [[NSMutableDictionary alloc] init];
	
	NSString* applicationSupportDirectoryPath = [self applicationSupportDirectory];
	NSString* satellitesPath = [applicationSupportDirectoryPath stringByAppendingPathComponent:@"satellites.xml"];
	
	NSXMLDocument* xmlDoc = [[NSXMLDocument alloc] initWithContentsOfURL:[NSURL fileURLWithPath:satellitesPath] options:NSXMLDocumentTidyXML error: &error] ;
	
	if (xmlDoc) {
		//XDebugLog(@"%@", xmlDoc);
		
		NSArray* satNodes = [xmlDoc nodesForXPath:@"/satellites/sat" error:&error];
		
		//loop on all XML elements
		for(int i=0; i<[satNodes count]; i++) {
			
			
			//get the element
			NSXMLElement* node = (NSXMLElement*)[satNodes objectAtIndex:i];
			
			//get attribute #1 name
			NSXMLNode* attrName = [node attributeForName:@"name"];

			//get attribute #2 position
			NSXMLNode* attrPosition = [node attributeForName:@"position"];
			
			[(NSMutableDictionary*)allSatellites setObject:[attrName stringValue] forKey:[attrPosition stringValue]];
			
		}
		
	}
	else {
		
		//either the reading failed, either it returned an non XML document
		[self reportFatalError:[NSString stringWithFormat:@"Can not retrieve read satellites from %@", satellitesPath] error:error];
		
	}
	//XDebugLog(@"AllSatellites = %@", allSatellites);
	XDebugLog(@"Parsed %d Satellites", [allSatellites count]);
}

// -------------------------------------------------------------------------------
//	parseUserBouquetFile:
//
//  parse the user bouqet file name to populte the userBouquets structure
// -------------------------------------------------------------------------------
-(void)parseUserBouquetFile:(NSString*)name {
	
	if (!userBouquets)
		userBouquets = [[NSMutableDictionary alloc] init];
	
	
	//skip radio bouquet fior the time being
	if ([name hasSuffix:@".radio"])
		return;
	
	XDebugLog(@"Parsing Bouquet = %@", name);

	NSError* error;
	
	NSString* applicationSupportDirectoryPath = [self applicationSupportDirectory];
	NSString* filePath = [applicationSupportDirectoryPath stringByAppendingPathComponent:name];
	
	NSString* bouquetFile = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
	
	if (!bouquetFile) {
		
		XLog(@"Can not read %@ from Application Support directory : %@", bouquetFile, filePath);
		[self reportFatalError:[NSString stringWithFormat:@"Can not read file %@", filePath] error:error];
		return;
	}
	
	NSMutableDictionary* bouquet = [[NSMutableDictionary alloc] init];
	[bouquet setObject:name forKey:DB_BOUQUET_FILE];
	
	NSArray* lines = [bouquetFile componentsSeparatedByString:@"\n"];
	NSUInteger cursor = 0;

	NSString* line = [[lines objectAtIndex:cursor]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	NSString* bouquetName = [line substringFromIndex:[@"#NAME " length]];
	[bouquet setObject:bouquetName forKey:DB_BOUQUET_NAME];
	//XDebugLog(@"bouquetName = %@", bouquetName);
	
	//next line
	cursor++;
	
	//must read every line for services
	NSMutableArray* services = [[NSMutableArray alloc] init];
	while (cursor < [lines count]) {

		//read the service line
		line = [lines objectAtIndex:cursor];
			
		if (![@"" isEqualToString:[line stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]]) //skip the last (empty line) 
		{
			NSString* bouquetService = [[line substringFromIndex:[@"#SERVICE " length]] uppercaseString];
			bouquetService = [bouquetService stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]; //remove end of line character
			//XDebugLog(@"bouquetService = %@", bouquetService);
			
			//find the corresponding service
			NSDictionary* service = [self serviceByServiceID:bouquetService];
			if (service == nil)
				service = [NSDictionary dictionaryWithObjectsAndKeys:@"Service unavailable", DB_SERVICE_NAME, bouquetService, DB_SERVICE_REF, nil];

			//add it to our list of services
			[services addObject:service];
		}
		
		cursor++;
	}
	
	[bouquet setObject:services forKey:DB_BOUQUET_SERVIVES];
	
	[(NSMutableDictionary*)userBouquets setObject:bouquet forKey:bouquetName];
	
}


@end
