//
//  Sidebar.m
//  iDream
//
//  Created by Sébastien Stormacq on 21/11/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import "Sidebar.h"
#import "ImageAndTextCell.h"
#import "log.h"

#define COLUMNID_NAME			@"NameColumn"	// the single column name in our outline view
#define UNTITLED_NAME			@"Untitled"		// default name for added folders and leafs

#define kNodesPBoardType		@"myNodesPBoardType"	// drag and drop pasteboard type



// -------------------------------------------------------------------------------
//	TreeAdditionObj
//
//	This object is used for passing data between the main and secondary thread
//	which populates the outline view.
// -------------------------------------------------------------------------------
@interface TreeAdditionObj : NSObject
{
	SidebarNode   *parentNode;  
	SidebarNode   *node;  
	NSIndexPath   *indexPath;
	NSImage		  *nodeImage;
	NSString	  *nodeName;
	NSDictionary  *nodeData;
	BOOL		  selectItsParent;
	BOOL		  leaf;
}

@property (retain)   SidebarNode   *node;
@property (retain)   NSIndexPath   *indexPath;
@property (readonly) SidebarNode   *parentNode;
@property (readonly) NSImage       *nodeImage;
@property (readonly) NSString      *nodeName;
@property (readonly) NSDictionary  *nodeData;
@property (readonly) BOOL selectItsParent;
@property (readonly) BOOL leaf;
@end

@implementation TreeAdditionObj
@synthesize node, parentNode, indexPath, nodeImage, nodeName, nodeData, selectItsParent, leaf;

// -------------------------------------------------------------------------------
- (id)initWithName:(NSString *)name andData:(NSDictionary*)nData andImage:(NSImage *)image andParent:(SidebarNode*)parent selectItsParent:(BOOL)select isLeaf:(BOOL)isLeaf
{
	self = [super init];
	
	parentNode      = parent;
	nodeName        = name;
	nodeData        = nData;
	nodeImage       = image;
	selectItsParent = select;
	leaf            = isLeaf;
	
	return self;
}
@end


@implementation Sidebar

@synthesize dragNodesArray;
@synthesize outlineView;

#pragma mark initialization

-(void)awakeFromNib {

	XDebugLog(@"Sidebar:awakeFromNib");

	contents = [[NSMutableArray alloc] init];

	// cache the reused icon images
	screenImage = [NSImage imageNamed:@"screen.png"];
	[screenImage setSize:NSMakeSize(16,16)];

	
	// apply our custom ImageAndTextCell for rendering the first column's cells
	//COLUMNID_NAME is used in the NIB file to identify the column
	NSTableColumn *tableColumn = [outlineView tableColumnWithIdentifier:COLUMNID_NAME];
	ImageAndTextCell *imageAndTextCell = [[[ImageAndTextCell alloc] init] autorelease];
	[imageAndTextCell setEditable:YES];
	[tableColumn setDataCell:imageAndTextCell];


	// scroll to the top in case the outline contents is very long
	[[[outlineView enclosingScrollView] verticalScroller] setFloatValue:0.0];
	[[[outlineView enclosingScrollView] contentView] scrollToPoint:NSMakePoint(0,0)];
	
	// make our outline view appear with gradient selection, and behave like the Finder, iTunes, etc.
	[outlineView setSelectionHighlightStyle:NSTableViewSelectionHighlightStyleSourceList];
	
	// drag and drop support
	[outlineView registerForDraggedTypes:[NSArray arrayWithObjects:
											kNodesPBoardType,			// our internal drag type
											nil]];	
	
}

#pragma mark NSTreeBinding Support
// -------------------------------------------------------------------------------
//	setContents:newContents
// -------------------------------------------------------------------------------
- (void)setContents:(NSArray*)newContents
{
	if (contents != newContents)
	{
		[contents release];
		contents = [[NSMutableArray alloc] initWithArray:newContents];
	}
}

// -------------------------------------------------------------------------------
//	contents:
// -------------------------------------------------------------------------------
- (NSMutableArray *)contents
{
	return contents;
}

#pragma mark debugging

-(NSString*)printIndexPath:(NSIndexPath*)path {
	
	NSUInteger indexes[[path length]];
	[path getIndexes:indexes];
	
	NSMutableString* result = [[NSMutableString alloc] init];
	[result appendFormat:@"- "];
	for(int i=0; i<[path length]; i++) {
		[result appendFormat:@"%d - ", indexes[i]];
	}
	
	return result;
}

-(BOOL)isSimilar:(NSIndexPath*)path1 toIndexPath:(NSIndexPath*)path2 {
	
	BOOL equal = ([path1 length] == [path2 length]);
	
	NSUInteger indexes1[[path1 length]];
	[path1 getIndexes:indexes1];

	NSUInteger indexes2[[path2 length]];
	[path2 getIndexes:indexes2];

	for(int i=0; equal && i<[path1 length]; i++) {
		equal = equal && (indexes1[i] == indexes2[i]);
	}
	
//	NSLog(@"isSimilar:toIndexPath: %@   ----   %@   == %d", [self printIndexPath:path1], [self printIndexPath:path2], equal);
//	if (equal) NSLog(@"Same Parent !"); else NSLog(@"NOT same parent !");
	
	return equal;
}


#pragma mark Data Manipulation

// -------------------------------------------------------------------------------
//	selectParentFromSelection:
//
//	Take the currently selected node and select its parent.
// -------------------------------------------------------------------------------
- (void)selectParentFromSelection
{
	if ([[treeController selectedNodes] count] > 0)
	{
		NSTreeNode* firstSelectedNode = [[treeController selectedNodes] objectAtIndex:0];
		NSTreeNode* parentNode = [firstSelectedNode parentNode];
		if (parentNode)
		{
			// select the parent
			NSIndexPath* parentIndex = [parentNode indexPath];
			[treeController setSelectionIndexPath:parentIndex];
		}
		else
		{
			// no parent exists (we are at the top of tree), so make no selection in our outline
			NSArray* selectionIndexPaths = [treeController selectionIndexPaths];
			[treeController removeSelectionIndexPaths:selectionIndexPaths];
		}
	}
}

// -------------------------------------------------------------------------------
//	performAddNode:treeAddition
// -------------------------------------------------------------------------------
-(void)performAddNode:(TreeAdditionObj *)treeAddition
{
	// NSTreeController inserts objects using NSIndexPath, so we need to calculate this
	NSIndexPath *indexPath = nil;
	
	// create a node
	SidebarNode *node;
	
	if (treeAddition.leaf) {
		node = [[SidebarNode alloc] initLeaf];
	} else {
		node = [[SidebarNode alloc] init];
	}
	[node setNodeTitle:[treeAddition nodeName]];
	[node setNodeData:[treeAddition nodeData]];
	[node setNodeIcon:[treeAddition nodeImage]];

	if (treeAddition.parentNode == nil) {
		
		//add at the top level
		indexPath = [NSIndexPath indexPathWithIndex:[contents count]];
		//NSLog(@"%@ : indexPath = %@", [treeAddition nodeName], [self printIndexPath:indexPath]);
		
	} else {
		
		//add under the desired parent
		indexPath = [[treeAddition.parentNode indexPathInArray:contents] indexPathByAddingIndex:[[treeAddition.parentNode children] count]];
		//NSLog(@"%@ : indexPath = %@", [treeAddition nodeName], [self printIndexPath:indexPath]);
		
	}
	
	// the user is adding a child node, tell the controller directly
	[treeController insertObject:node atArrangedObjectIndexPath:indexPath];

	treeAddition.indexPath = indexPath;
	treeAddition.node = node;

	[node release];
	
	// adding a child automatically becomes selected by NSOutlineView, so keep its parent selected
	if ([treeAddition selectItsParent])
		[self selectParentFromSelection];
	
}

// -------------------------------------------------------------------------------
//	addNode:treeAddition
// -------------------------------------------------------------------------------
-(SidebarNode*)addNode:(TreeAdditionObj *)treeObjInfo {

	if (buildingOutlineView)
	{
		// add the child node to the tree controller, but on the main thread to avoid lock ups
		[self performSelectorOnMainThread:@selector(performAddNode:) withObject:treeObjInfo waitUntilDone:YES];
	}
	else
	{
		[self performAddNode:treeObjInfo];
	}
	
	return treeObjInfo.node;
}

// -------------------------------------------------------------------------------
//	addChild:withImage:parentNode:
// -------------------------------------------------------------------------------
- (SidebarNode*)addChild:(NSDictionary*)child withImage:(NSImage*)image parentNode:(SidebarNode*)parent action:(SEL)action target:(id)target editable:(BOOL)canEdit idValue:(NSString*)idValue
{
	TreeAdditionObj *treeObjInfo = [[TreeAdditionObj alloc] initWithName:[child objectForKey:idValue] andData:child andImage:image andParent:parent selectItsParent:NO isLeaf:YES];
	
	SidebarNode* result = [self addNode:treeObjInfo];
	result.target     = target;
	result.action     = action;
	result.isEditable = canEdit;
	
	[treeObjInfo release];
	return result;
	
}

// -------------------------------------------------------------------------------
//	addChild:withImage:parentNode:
// -------------------------------------------------------------------------------
- (SidebarNode*)addChild:(NSString *)childName withImage:(NSImage*)image parentNode:(SidebarNode*)parent action:(SEL)action target:(id)target editable:(BOOL)canEdit;
{
	TreeAdditionObj *treeObjInfo = [[TreeAdditionObj alloc] initWithName:childName andData:nil andImage:image andParent:parent selectItsParent:NO isLeaf:YES];

	SidebarNode* result = [self addNode:treeObjInfo];
	result.target     = target;
	result.action     = action;
	result.isEditable = canEdit;
	
	[treeObjInfo release];
	return result;
}


// -------------------------------------------------------------------------------
//	addFolder:folderName:andImage
// -------------------------------------------------------------------------------
- (SidebarNode*)addFolder:(NSString *)folderName withImage:(NSImage*)image parentNode:(SidebarNode*)parent 
{
	TreeAdditionObj *treeObjInfo = [[TreeAdditionObj alloc] initWithName:folderName andData:nil andImage:image andParent:parent selectItsParent:NO isLeaf:NO];
	
	SidebarNode* result = [self addNode:treeObjInfo];
	
	[treeObjInfo release];
	return result;
}

// -------------------------------------------------------------------------------
//	addFolder:addRootFolder:
// -------------------------------------------------------------------------------
- (SidebarNode*)addRootFolder:(NSString *)folderName
{
	[treeController setSelectionIndexPath:nil];
	return [self addFolder:folderName withImage:nil parentNode:nil];
}

// -------------------------------------------------------------------------------
//	startPopulatingContent:
//
//	This method is being called to notify the OutlineView we will build the content
//  Usually this is done at startup time
// -------------------------------------------------------------------------------
- (void)startPopulatingContent  {

	buildingOutlineView = YES;		// indicate to ourselves we are building the default tree at startup
	[outlineView setHidden:YES];	// hide the outline view - don't show it as we are building the contents
}
	
// -------------------------------------------------------------------------------
//	stopPopulatingContent:
//
//	This method is being called to notify the OutlineView we finished building the content
//  Usually this is done at startup time
// -------------------------------------------------------------------------------
- (void)stopPopulatingContent  {
		
	buildingOutlineView = NO;		// we're done building our default tree
	[outlineView setHidden:NO];	// we are done populating the outline view content, show it again
	
	// scroll to the top in case the outline contents is very long
	//[[[outlineView enclosingScrollView] verticalScroller] setFloatValue:0.0];
	//[[[outlineView enclosingScrollView] contentView] scrollToPoint:NSMakePoint(0,0)];
	
}

// ----------------------------------------------------------------------------------------
//  removeAnySelection:
//
//  This metod will remove any selection from the Outline View
// ----------------------------------------------------------------------------------------
-(void)removeAnySelection {
	// remove the current selection
	NSArray *selection = [treeController selectionIndexPaths];
	[treeController removeSelectionIndexPaths:selection];
}	

// -------------------------------------------------------------------------------
//	removeNode:
//
//	remove the currently selected node
// -------------------------------------------------------------------------------
- (void)removeSelectedNode {
	
	
	//currently selected IndexPath
	NSIndexPath* indexPath = [treeController selectionIndexPath];
	//NSLog(@"current indexPath = %@",[self printIndexPath:indexPath]);
	
	//get the last index from the indexPath
	NSUInteger indexes[[indexPath length]];
	[indexPath getIndexes:indexes];
	NSUInteger lastIndex = indexes[[indexPath length] - 1];
	//NSLog(@"lastIndex = %d", lastIndex);
	
	//remove the node
	[treeController remove:self];

	//reselect the node just above the one deleted
	indexPath = [indexPath indexPathByRemovingLastIndex]; //remove the last index
	indexPath = [indexPath indexPathByAddingIndex:lastIndex - 1]; //add as last index, the position just before
	//NSLog(@"new indexPath    = %@",[self printIndexPath:indexPath]);
	
	[treeController setSelectionIndexPath:indexPath];
}
	 
// -------------------------------------------------------------------------------
//	selectedNode:
//
//	return the currently selected node, nil if no selection
// -------------------------------------------------------------------------------

 - (SidebarNode*)selectedNode {
	 
	 SidebarNode* result = nil;
	 
	 if ([[treeController selectedNodes] count] == 1) {
		 
		 result = [[[treeController selectedNodes] objectAtIndex:0] representedObject];
	 }
	 
	 return result;
 }
	 
// -------------------------------------------------------------------------------
//	collapseNode:
//
// -------------------------------------------------------------------------------
- (void)collapseNode:(SidebarNode*)node {
	[outlineView collapseItem:node];
}

#pragma mark - NSOutlineView drag and drop

// ----------------------------------------------------------------------------------------
// draggingSourceOperationMaskForLocal <NSDraggingSource override>
// ----------------------------------------------------------------------------------------
- (NSDragOperation)draggingSourceOperationMaskForLocal:(BOOL)isLocal
{
	return NSDragOperationMove;
}

// ----------------------------------------------------------------------------------------
// outlineView:writeItems:toPasteboard
// ----------------------------------------------------------------------------------------
- (BOOL)outlineView:(NSOutlineView *)ov writeItems:(NSArray *)items toPasteboard:(NSPasteboard *)pboard
{
	[pboard declareTypes:[NSArray arrayWithObjects:kNodesPBoardType, nil] owner:self];
	
	// keep track of this nodes for drag feedback in "validateDrop"
	self.dragNodesArray = items;
	
	return YES;
}

// -------------------------------------------------------------------------------
//	outlineView:validateDrop:proposedItem:proposedChildrenIndex:
//
//	This method is used by NSOutlineView to determine a valid drop target.
// -------------------------------------------------------------------------------
- (NSDragOperation)outlineView:(NSOutlineView *)ov validateDrop:(id <NSDraggingInfo>)info proposedItem:(id)item proposedChildIndex:(NSInteger)index
{
	NSDragOperation result = NSDragOperationNone;
	
	if (!item)
	{
		// no item to drop on
		result = NSDragOperationNone;
	}
	else
	{
		//find source and target indexPath 
		//authorize drop only if source and target have the same parent
		BOOL sameParent = NO;
		
		//first get the target
		NSTreeNode* targetNode = item;
		
		//then get the source
		NSPasteboard *pboard = [info draggingPasteboard];	// get the pasteboard
		if ([pboard availableTypeFromArray:[NSArray arrayWithObject:kNodesPBoardType]]) {
			NSArray *nodes = self.dragNodesArray;
			NSTreeNode* sourceNode = [nodes objectAtIndex:0];			//assume only one node is dragged

			NSIndexPath* targetParentIndexPath = [targetNode indexPath] ;
			NSIndexPath* sourceParentIndexPath = [[sourceNode indexPath] indexPathByRemovingLastIndex];
			//NSLog(@"targetNode IP = %@=%@", [[targetNode representedObject] nodeTitle], [self printIndexPath:targetParentIndexPath]);
			//NSLog(@"sourceNode IP = %@=%@", [[sourceNode representedObject] nodeTitle], [self printIndexPath:sourceParentIndexPath]);
			
			sameParent = [self isSimilar:targetParentIndexPath toIndexPath:sourceParentIndexPath];
		}
		
		if (index == -1) {
			// don't allow dropping on a child
			result = NSDragOperationNone;
	    }
		else if (!sameParent) {
			// don't allow dropping when nodes have different parents 
			result = NSDragOperationNone;
		} else {
			// drop location is a container
			result = NSDragOperationMove;
		}

	}
	
	return result;
}


// -------------------------------------------------------------------------------
//	handleDrops:pboard:withIndexPath:
//
//	The user is doing an intra-app drag within the outline view.
// -------------------------------------------------------------------------------
- (void)handleDrops:(NSPasteboard*)pboard withIndexPath:(NSIndexPath*)indexPath
{
	// user is doing an intra app drag within the outline view:
	//
	NSArray* newNodes = self.dragNodesArray;
	
	// move the items to their new place (we do this backwards, otherwise they will end up in reverse order)
	NSInteger i;
	for (i = ([newNodes count] - 1); i >=0; i--)
	{
		[treeController moveNode:[newNodes objectAtIndex:i] toIndexPath:indexPath];
	}
	
	// keep the moved nodes selected
	NSMutableArray* indexPathList = [NSMutableArray array];
	for (i = 0; i < [newNodes count]; i++)
	{
		[indexPathList addObject:[[newNodes objectAtIndex:i] indexPath]];
	}
	[treeController setSelectionIndexPaths: indexPathList];
}


// -------------------------------------------------------------------------------
//	outlineView:acceptDrop:item:childIndex
//
//	This method is called when the mouse is released over an outline view that previously decided to allow a drop
//	via the validateDrop method. The data source should incorporate the data from the dragging pasteboard at this time.
//	'index' is the location to insert the data as a child of 'item', and are the values previously set in the validateDrop: method.
//
// -------------------------------------------------------------------------------
- (BOOL)outlineView:(NSOutlineView*)ov acceptDrop:(id <NSDraggingInfo>)info item:(id)targetItem childIndex:(NSInteger)index
{
	// note that "targetItem" is a NSTreeNode proxy
	//
	BOOL result = NO;
	
	// find the index path to insert our dropped object(s)
	NSIndexPath *indexPath;
	if (targetItem)
	{
		// drop down inside the tree node:
		// feth the index path to insert our dropped node
		indexPath = [[targetItem indexPath] indexPathByAddingIndex:index];
	}
	else
	{
		// drop at the top root level
		if (index == -1)	// drop area might be ambibuous (not at a particular location)
			indexPath = [NSIndexPath indexPathWithIndex:[contents count]];		// drop at the end of the top level
		else
			indexPath = [NSIndexPath indexPathWithIndex:index]; // drop at a particular place at the top level
	}
	
	NSPasteboard *pboard = [info draggingPasteboard];	// get the pasteboard
	
	// check the dragging type -
	if ([pboard availableTypeFromArray:[NSArray arrayWithObject:kNodesPBoardType]])
	{
		// user is doing an intra-app drag within the outline view
		[self handleDrops:pboard withIndexPath:indexPath];
		result = YES;
	}

	return result;
}


#pragma mark - NSOutlineView delegate

// ----------------------------------------------------------------------------------------
// outlineView:isGroupItem:item
// ----------------------------------------------------------------------------------------
-(BOOL)outlineView:(NSOutlineView*)outlineView isGroupItem:(id)item
{
	//root nodes are group items
	NSTreeNode* node = (NSTreeNode*)item;
	return [[node indexPath] length] == 1;
}

// -------------------------------------------------------------------------------
//	shouldSelectItem:item
// -------------------------------------------------------------------------------
- (BOOL)outlineView:(NSOutlineView *)outlineView shouldSelectItem:(id)item;
{
	//only non root nodes are selectable
	NSTreeNode* node = (NSTreeNode*)item;
	return [[node indexPath] length] > 1;
}

// -------------------------------------------------------------------------------
//	outlineViewSelectionDidChange:notification
// -------------------------------------------------------------------------------
- (void)outlineViewSelectionDidChange:(NSNotification *)notification {
	if (buildingOutlineView) return;
	
	NSArray *selection = [treeController selectedObjects];
	if ([selection count] < 1) return;
	
	// Run Thread with selected Action
	SidebarNode *node = [selection objectAtIndex:0];
	if (node.action)
		[NSThread detachNewThreadSelector:[node action] toTarget:[node target] withObject:node];
}


// -------------------------------------------------------------------------------
//	shouldEditTableColumn:tableColumn:item
//
//	Decide to allow the edit of the given outline view "item".
// -------------------------------------------------------------------------------
- (BOOL)outlineView:(NSOutlineView *)theOutlineView shouldEditTableColumn:(NSTableColumn *)tableColumn item:(id)item
{
	SidebarNode* node= (SidebarNode*)[item representedObject];
	
	BOOL result = node.isEditable;
	
	if (result)
		[outlineView editColumn:0 row:[outlineView selectedRow] withEvent:[NSApp currentEvent] select:YES];
	
	return result;
}


// -------------------------------------------------------------------------------
//	textShouldEndEditing:
// -------------------------------------------------------------------------------
- (BOOL)control:(NSControl *)control textShouldEndEditing:(NSText *)fieldEditor
{
	if ([[fieldEditor string] length] == 0)
	{
		// don't allow empty node names
		return NO;
	}
	else
	{
		return YES;
	}
}


// -------------------------------------------------------------------------------
//	outlineView:willDisplayCell
// -------------------------------------------------------------------------------
- (void)outlineView:(NSOutlineView *)olv willDisplayCell:(NSCell*)cell forTableColumn:(NSTableColumn *)tableColumn item:(id)item
{	 
	if ([[tableColumn identifier] isEqualToString:COLUMNID_NAME])
	{
		// we are displaying the single and only column
		if ([cell isKindOfClass:[ImageAndTextCell class]])
		{
			SidebarNode* node = (SidebarNode*)[item representedObject];
			
			// set the cell's image
			//NSLog(@"willDisplayCell %@ : %@", [node nodeTitle], [[node nodeIcon] name]);
			[(ImageAndTextCell*)cell setImage:[node nodeIcon]];
		}
	}
}


@end
