//
//  SidebarNode.h
//  iDream
//
//  Created by Sébastien Stormacq on 21/11/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SidebarNode : NSObject <NSCoding, NSCopying>
{
	NSString		*nodeTitle;
	NSDictionary	*nodeData;
	NSMutableArray	*children;
	BOOL			isLeaf;
	NSImage			*nodeIcon;
	
	SEL				action;
	id				target;
	
	BOOL			isEditable;
}

@property (retain) NSString* nodeTitle;
@property (retain) NSImage*  nodeIcon; 
@property (retain) NSDictionary *nodeData;
@property (assign) SEL action; 
@property (retain) id target; 
@property (assign) BOOL isEditable;

- (id)initLeaf;

- (void)setChildren:(NSArray*)newChildren;
- (NSMutableArray*)children;

- (void)setLeaf:(BOOL)flag;
- (BOOL)isLeaf;

- (void)setNodeIcon:(NSImage*)icon;
- (NSImage*)nodeIcon;

- (BOOL)isDraggable;

- (NSComparisonResult)compare:(SidebarNode*)aNode;

- (NSArray*)mutableKeys;

- (NSDictionary*)dictionaryRepresentation;
- (id)initWithDictionary:(NSDictionary*)dictionary;

- (id)parentFromArray:(NSArray*)array;
- (void)removeObjectFromChildren:(id)obj;
- (NSArray*)descendants;
- (NSArray*)allChildLeafs;
- (NSArray*)groupChildren;
- (BOOL)isDescendantOfOrOneOfNodes:(NSArray*)nodes;
- (BOOL)isDescendantOfNodes:(NSArray*)nodes;
- (NSIndexPath*)indexPathInArray:(NSArray*)array;

@end
