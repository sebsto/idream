//
//  iDreamAppDelegate.m
//  iDream
//
//  Created by Sebastien Stormacq on 16/11/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import "iDreamAppDelegate.h"
#import "MainWindowController.h"
#import "log.h"

#define SETTINGS_WINDOW_NIBNAME		@"SettingsWindow"		//nib file for the settings Window
#define MAIN_WINDOW_NIBNAME			@"MainWindow"			//nib file for the main window
#define ABOUT_WINDOW_NIBNAME		@"AboutWindow"			//nib file for the about window

#define PREFS_KEY_WINDOW			@"window"				//prefs key for window position and size
#define PREFS_DREAMBOX_SETTINGS     @"dreambox"				//prefs key for the dreambox settings

@implementation iDreamAppDelegate


- (void)awakeFromNib {
	XDebugLog(@"iDreamAppDelegate - awakeFromNib");

	//reading defaults
	prefs = [NSUserDefaults standardUserDefaults];
	
	//TODO : check if preferences exist or not
	//when they don't start the initial settings wizard

	//initialize the DreamboxManager, retrieving DB files and parsing will be executed in a separate thread
	dbManager = [DreamboxManager dreamboxManagerWithDictionary:[prefs dictionaryForKey:PREFS_DREAMBOX_SETTINGS]];

	//load the main Windows
	mainWindowController = [[MainWindowController alloc] initWithWindowNibName:MAIN_WINDOW_NIBNAME];

	//restore window size and pos
	[mainWindowController.window setFrameUsingName:PREFS_KEY_WINDOW force:YES];
	[mainWindowController showWindow:self];
	mainWindowController.dbManager = dbManager;
	dbManager.delegate = mainWindowController;
	
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {

	XDebugLog(@"iDreamAppDelegate - applicationDidFinishLaunching");
	
	//notify the main window controller
	//so that it can start loading the header and sidebar data
	[mainWindowController applicationDidFinishLaunching];

}

/**
 * Tell the application to qui when the last window is closed
 * This method is part of the Application delegate
 */
-(BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication*)app {
	return YES;
}


- (IBAction)editPreferences:(id)sender {

	//TODO : should use KeyStore to store password

	if (!settingsController)
		settingsController = [[SettingsController alloc] initWithWindowNibName:SETTINGS_WINDOW_NIBNAME];
		
	// ask our settings sheet for information on the new child to be added
	NSMutableDictionary *newValues = [settingsController edit:[prefs dictionaryForKey:PREFS_DREAMBOX_SETTINGS] from:mainWindowController];
	if (![settingsController wasCancelled] && newValues)
	{
		[prefs setObject:newValues forKey:PREFS_DREAMBOX_SETTINGS];
	}
}

- (IBAction)showAboutWindow:(id)sender {
	
	if (!aboutWindowController)
		aboutWindowController = [[AboutWindowController alloc] initWithWindowNibName:ABOUT_WINDOW_NIBNAME];
	
	[aboutWindowController showAboutBox];

	
}

@end
