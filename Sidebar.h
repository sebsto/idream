//
//  Sidebar.h
//  iDream
//
//  Created by Sébastien Stormacq on 21/11/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SidebarNode.h"


@interface Sidebar : NSViewController <NSOutlineViewDelegate, NSOutlineViewDataSource> {

	IBOutlet NSOutlineView      *outlineView;
	IBOutlet NSTreeController	*treeController;
	
	
	NSMutableArray				*contents;				//NSTreeBinding suport

	NSArray						*dragNodesArray;		// used to keep track of dragged nodes

	BOOL						buildingOutlineView;	// signifies we are building the outline view at launch time
	 
	NSImage						*screenImage;			// cached image for generic bouquet
	
}

@property (retain) NSArray *dragNodesArray;
@property (readonly) NSOutlineView      *outlineView;

- (void)setContents:(NSArray*)newContents;
- (NSMutableArray*)contents;

- (SidebarNode*)selectedNode;
- (void)removeSelectedNode;
- (SidebarNode*)addChild:(NSDictionary*)child withImage:(NSImage*)image parentNode:(SidebarNode*)parent action:(SEL)action target:(id)target editable:(BOOL)canEdit idValue:(NSString*)idValue;
- (SidebarNode*)addChild:(NSString *)childName withImage:(NSImage*)image parentNode:(SidebarNode*)parent action:(SEL)action target:(id)target editable:(BOOL)canEdit;
- (SidebarNode*)addFolder:(NSString *)folderName withImage:(NSImage*)image parentNode:(SidebarNode*)parent;
- (SidebarNode*)addRootFolder:(NSString *)folderName;
- (void)collapseNode:(SidebarNode*)node;
- (void)startPopulatingContent;
- (void)stopPopulatingContent;
- (void)removeAnySelection;

@end
