//
//  HeaderViewController.m
//  iDream
//
//  Created by Sebastien Stormacq on 16/11/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import "HeaderViewController.h"
#import "log.h"

#define DB_INFO_TYPE		@"dbType"
#define DB_INFO_VERSION		@"dbVersion"
#define DB_INFO_MODEL		@"dbModel"

@implementation HeaderViewController

// -------------------------------------------------------------------------------
//	awakeFromNib:
// -------------------------------------------------------------------------------
-(void)awakeFromNib {
	
	XDebugLog(@"HeaderView awakeFromNib");
	
}

// -------------------------------------------------------------------------------
//	setDreamboxInfo:
// -------------------------------------------------------------------------------
-(void)setDreamboxInfo:(NSDictionary*)info {

	//error while reading DB info
	if ([info count] == 0) {
		[self setProgressStatus:NO];
		return;
	}
	
	
	[lbDBType setStringValue:[info objectForKey:DB_INFO_TYPE]];
	[lbDBVersion setStringValue:[info objectForKey:DB_INFO_VERSION]];
	
	ivDB.image = [NSImage imageNamed:[NSString stringWithFormat:@"%@.png", [info objectForKey:DB_INFO_MODEL]]];
	
	if (!ivDB.image)
		ivDB.image = [NSImage imageNamed:@"dream-logo.png"];
}

// -------------------------------------------------------------------------------
//	setStatus:
// -------------------------------------------------------------------------------
-(void)setStatus:(NSString*)newStatus {
	[lbDBStatus setStringValue:newStatus];
}

// -------------------------------------------------------------------------------
//	setStatus:
// -------------------------------------------------------------------------------
-(void)setProgressStatus:(BOOL)animated {

	if (animated) {
		[ivDB setHidden:YES];
		[progress setHidden:NO];
		[progress startAnimation:self];
	} else { 
		[progress stopAnimation:self];
		[progress setHidden:YES];
		[ivDB setHidden:NO];
	}	
	
}

@end
