//
//  HeaderViewController.h
//  iDream
//
//  Created by Sebastien Stormacq on 16/11/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface HeaderViewController : NSViewController {

	IBOutlet NSTextField* lbDBType;
	IBOutlet NSTextField* lbDBVersion;
	IBOutlet NSTextField* lbDBStatus;
	
	IBOutlet NSImageView* ivDB;

	IBOutlet NSProgressIndicator* progress;

}

-(void)setProgressStatus:(BOOL)animated;
-(void)setStatus:(NSString*)newStatus;
-(void)setDreamboxInfo:(NSDictionary*)info;

@end
