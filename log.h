/*
 *  log.h
 *  iDream
 *
 *  Created by Sébastien Stormacq on 26/11/09.
 *  Copyright 2009 Sun Microsystems. All rights reserved.
 *
 */

//Xlog macro replace NSLog
#define XLog(...) fprintf(stderr, "%s iDream[%u] %s\n", [[[NSDate date] descriptionWithCalendarFormat:@"%Y%m%d - %H%M%S.%F" timeZone:nil locale:nil] UTF8String], getpid(), [[NSString stringWithFormat:@"%@:%u: %@", [[NSString stringWithCString:__FILE__  encoding:NSUTF8StringEncoding] lastPathComponent], __LINE__, [NSString stringWithFormat:__VA_ARGS__]] UTF8String]);

//XDebugLog replace NSLog but produces output only when debug=yes in the preference file
#define XDebugLog(...) if([[NSUserDefaults standardUserDefaults] boolForKey:@"debug"]) XLog(__VA_ARGS__);