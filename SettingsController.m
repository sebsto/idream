//
//  SettingsController.m
//  iDream
//
//  Created by Sébastien Stormacq on 23/11/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import "SettingsController.h"
#import "DreamboxManager.h"
#import "log.h"


@implementation SettingsController

// -------------------------------------------------------------------------------
//	init:
// -------------------------------------------------------------------------------
- (id)init
{
	self = [super init];
	
	return self;
}

// -------------------------------------------------------------------------------
//	windowNibName:
// -------------------------------------------------------------------------------
- (NSString*)windowNibName
{
	return @"SettingsWindow";
}

// -------------------------------------------------------------------------------
//	dealloc:
// -------------------------------------------------------------------------------
- (void)dealloc
{
	[super dealloc];
	[savedFields release];
}

// -------------------------------------------------------------------------------
//	edit:startingValues:from
// -------------------------------------------------------------------------------
- (NSMutableDictionary*)edit:(NSDictionary*)startingValues from:(MainWindowController*)sender
{
	
	
	[ivValidAdress setHidden:YES];
	[ivValidPassword setHidden:YES];
	
	NSWindow* window = [self window];
	
	cancelled = NO;
	
	NSArray* editFields = [settingsForm cells];
	if (startingValues != nil)
	{
		// we are editing current entry, use its values as the default
		savedFields = [startingValues retain];
		
		[[editFields objectAtIndex:0] setStringValue:[startingValues objectForKey:@"address"]];
		[[editFields objectAtIndex:1] setStringValue:[startingValues objectForKey:@"username"]];
		[[editFields objectAtIndex:2] setStringValue:[startingValues objectForKey:@"password"]];
	}
	else
	{
		// we are adding a new entry,
		// make sure the form fields are empty due to the fact that this controller is recycled
		// each time the user opens the sheet -
		[[editFields objectAtIndex:0] setStringValue:@""];
		[[editFields objectAtIndex:1] setStringValue:@""];
		[[editFields objectAtIndex:2] setStringValue:@""];
	}
	
	[NSApp beginSheet:window modalForWindow:[sender window] modalDelegate:nil didEndSelector:nil contextInfo:nil];
	[NSApp runModalForWindow:window];
	// sheet is up here...
	
	[NSApp endSheet:window];
	[window orderOut:self];
	
	return savedFields;
}

// -------------------------------------------------------------------------------
//	done:sender
// -------------------------------------------------------------------------------
- (IBAction)done:(id)sender
{
	NSArray* editFields = [settingsForm cells];
	savedFields = [NSMutableDictionary dictionaryWithObjectsAndKeys:
				   [[editFields objectAtIndex:0] stringValue], @"address",
				   [[editFields objectAtIndex:1] stringValue], @"username",
				   [[editFields objectAtIndex:2] stringValue], @"password",
				   nil];
	[savedFields retain];
	
	[NSApp stopModal];
}

// -------------------------------------------------------------------------------
//	cancel:sender
// -------------------------------------------------------------------------------
- (IBAction)cancel:(id)sender
{
	[NSApp stopModal];
	cancelled = YES;
}

// -------------------------------------------------------------------------------
//	wasCancelled:
// -------------------------------------------------------------------------------
- (BOOL)wasCancelled
{
	return cancelled;
}

// -------------------------------------------------------------------------------
//	controlTextDidChange:
// -------------------------------------------------------------------------------
-(void)controlTextDidChange:(NSNotification *)obj {
	NSFormCell* cell     = [settingsForm selectedCell]; 

	if ([cell tag] == 0) {
		[ivValidAdress setHidden:YES];
		[ivValidAdress setImage:nil];
	}
}

// -------------------------------------------------------------------------------
//	controlTextDidEndEditing:
// -------------------------------------------------------------------------------
- (void)controlTextDidEndEditing:(NSNotification *)aNotification
{
	//NSTextView* textView = [[aNotification userInfo] objectForKey:@"NSFieldEditor"];
	NSFormCell* cell     = [settingsForm selectedCell]; 
	//NSString* address   = [textView string];
	XDebugLog(@"controlTextDidEndEditing for control %@ (%d) : %@ ", [cell title], [cell tag], [cell stringValue]);
	
	
	if ([cell tag] == 0 && [ivValidAdress isHidden]) {
		[NSThread detachNewThreadSelector:@selector(asyncValidateAddress:) toTarget:self withObject:cell];
	}
}

-(void)asyncValidateAddress:(id)cell {

	//NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init]; //not used anymore with GarbageCollection
	
	[pgAddress setHidden:NO];
	[pgAddress startAnimation:self];
	
	NSString* errorMsg = nil;
	
	BOOL result = [DreamboxManager validateAddress:[cell stringValue] error:&errorMsg];
	
	if (result) {
		[ivValidAdress setImage:[NSImage imageNamed:@"green-check-mark.png"]];
		[ivValidAdress setToolTip:@"OK"];
	} else {
		[ivValidAdress setImage:[NSImage imageNamed:@"red-check-mark.png"]];
		[ivValidAdress setToolTip:errorMsg];
	}

	
	[pgAddress stopAnimation:self];
	[pgAddress setHidden:YES];
	[ivValidAdress setHidden:NO];
	
	
	//[pool release];
}

@end
