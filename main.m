//
//  main.m
//  iDream
//
//  Created by Sebastien Stormacq on 16/11/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
	//DEBUG flag is defined in compiler settings (see debug build options)
	//When defined, all ouput are sent to the console
	//when not defined (Release build), all output are sent to the iDream log file
#ifndef DEBUG
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	
	NSString *logPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Logs/iDream.log"];
	freopen([logPath fileSystemRepresentation], "w", stderr);
	
	[pool release];
#endif	
    return NSApplicationMain(argc,  (const char **) argv);
}
