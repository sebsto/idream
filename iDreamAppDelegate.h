//
//  iDreamAppDelegate.h
//  iDream
//
//  Created by Sebastien Stormacq on 16/11/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SettingsController.h"
#import "DreamboxManager.h"
#import "AboutWindowController.h"

@class MainWindowController;

@interface iDreamAppDelegate : NSObject <NSApplicationDelegate> {

	DreamboxManager*		dbManager;					//the only DreamboxManager
	SettingsController*		settingsController;			//the controller for the settings panel
	MainWindowController*	mainWindowController;		//the controller for the main window
	AboutWindowController*  aboutWindowController;		//the about window 
	
	NSUserDefaults*			prefs;						// the user preferences
	
	
}

- (IBAction)editPreferences:(id)sender;
- (IBAction)showAboutWindow:(id)sender;

@end
