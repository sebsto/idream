//
//  ChannelsViewController.h
//  iDream
//
//  Created by Sébastien Stormacq on 21/11/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Sidebar.h"
#import "SettingsController.h";
#import "DreamboxManager.h"

@interface ChannelsViewController : NSViewController <NSTableViewDelegate, NSTableViewDataSource> {
	
	
@private	
	//these three outlets are used for the show/hide animations
	IBOutlet NSTabView*		tabView;				   
	IBOutlet NSSplitView*	splitView;
	IBOutlet NSView*		waitPane;
	
	IBOutlet NSButton *btAddBouquet;
	IBOutlet NSButton *btRemoveBouquet;
	
	IBOutlet NSSearchField *searchField;
	NSArray* initialData;								//data copy when searching
	
	IBOutlet Sidebar*		sidebar;					//gives direct access to theoutline view

	SettingsController* settingsController;
	
	//data storage
	SidebarNode* bouquetNode;
	
	//dreambox communication
	DreamboxManager* dbManager;
	
	//-------------------------------
	//
	// Table Management stuffs
	//
	//-------------------------------
	IBOutlet NSTableView*			serviceTable;			//the table view
	IBOutlet NSTextField*			lbNumberOfServices;		// number of services actually showed in the table
	NSMutableArray*					serviceData;			//the components to be displayed in the table

	//private variables to handle sorting
	NSUInteger columnSorted;		//0 = NAME, 1 = PROVIDER, 2 = SATELLITE
	BOOL	   ascendingOrder;	    //YES = SORTED ascending
	
}

@property (retain) DreamboxManager* dbManager;
@property (copy) NSMutableArray*	contents;

- (IBAction)addUserBouquet:(id)sender;
- (IBAction)removeUserBouquet:(id)sender;

//Table Data Source
- (NSInteger)numberOfRowsInTableView:(NSTableView *)aTableView;
- (id)tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex;

@end
