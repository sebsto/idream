//
//  MainWindowController.h
//  iDream
//
//  Created by Sébastien Stormacq on 23/11/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ChannelsViewController.h"
#import "HeaderViewController.h"
#import "DreamboxManager.h"

@interface MainWindowController : NSWindowController <DreamboxDelegate> {

	DreamboxManager* dbManager;									//the dreambox communication interface
	IBOutlet HeaderViewController*   headerViewController;		//the header view part of the MainWindow
	IBOutlet ChannelsViewController* channelsViewController;	//the channels part of the MainWindows
	
	NSInteger	indicateProgressCount;
}

@property (retain) DreamboxManager* dbManager;

-(void)updateStatusWithMessage:(NSString*)msg;
-(void)indicateProgress:(BOOL)show;
-(void)applicationDidFinishLaunching;

@end
