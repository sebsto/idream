//
//  SettingsController.h
//  iDream
//
//  Created by Sébastien Stormacq on 23/11/09.
//  Copyright 2009 Sun Microsystems. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class MainWindowController;

@interface SettingsController : NSWindowController {

	IBOutlet NSForm*	 settingsForm;
	
	IBOutlet NSImageView* ivValidAdress;
	IBOutlet NSImageView* ivValidPassword;
	
	IBOutlet NSButton* btCancel;
	IBOutlet NSButton* btOK;
	
	IBOutlet NSProgressIndicator* pgAddress;
	IBOutlet NSProgressIndicator* pgPassword;

	BOOL					cancelled;
	NSMutableDictionary*	savedFields;
	
}

- (NSMutableDictionary*)edit:(NSDictionary*)startingValues from:(MainWindowController*)sender;
- (BOOL)wasCancelled;

- (IBAction)done:(id)sender;
- (IBAction)cancel:(id)sender;

@end
